# Pay My Buddy

This is project is a money transfer application based on:

- an api build with [spring boot](https://github.com/spring-projects/spring-boot)
- a web interface build with [angular](https://github.com/angular/angular)

Main features:

- Account registration with a unique email address
- Authentication using jwt (json web token)
- A rest api for data fetching
- Spring jpa and mysql database
- The architecture is based on mvc pattern
- **Javadoc** & **jacoco test coverage reports** are available in the `/target` directory

## Run the project on your machine

## Requirements

- [Mysql ^8.0.0]((https://dev.mysql.com/downloads/))
- [JDK 11](https://adoptopenjdk.net/)
- [Maven ~4.0.0](https://maven.apache.org/install.html)

## 1) If not already done, install mysql server:

[Select the proper installer for your operating system](https://dev.mysql.com/downloads/)

Launch the installer and hit next until it finishes installing (make sure to check `run mysql server` before quitting
the installer.

You may also install mysql shell and add it to your pass if you don't have a mysql gui client

## 2) Clone this repository

```shell
git clone https://gitlab.com/abdoum/paymybuddy
```

## 2) Create the schema and populate it with fake data

```shell
cd paymybuddy/src/main/resources
mysql -u {replace_by_your_mysql_username} -p < schema.sql
mysql -u {replace_by_your_mysql_username} -p < data.sql
```

## 3) setup your database credentials

Add your credentials to 'src/main/resources/application.yml' and 'src/test/resources/application.yml' file. Make sure to
use a different database for running tests !!

```properties
spring.datasource.url=DEC(yourDatabaseUrl)
spring.datasource.username=DEC(yourUsername)
spring.datasource.password=DEC(yourPassword)
```

## 4) Run the following command in your terminal (make sure you are in the root directory of the project)

```shell
mvn jasypt:encrypt -Djasypt.encryptor.password=<replace this with your custom password>
```

This command should result in something similar to the following being generated in your 'application.properties' file

```properties
spring.datasource.url=ENC(raqbE8JG/dQd+kW66/FWVnPsvkmyJDr9lOepZU0GSuCTY9QfGf8kioIgQeiGtSVoUOEVoa++qFKr7L7/ldMPbMg)
spring.datasource.username=ENC(REFEFreferFRERGefhJIyjuyjkujtryhrt+2xKLGfHfQ7DLDK)
spring.datasource.password=ENC(AW/zZhWHhOH36D4BIdm3dn+GTRgrtevdegtqegjiktrojERGtyjyikgYT)
```

## 5) Run the application from your ide, and the encrypted properties should be decrypted at runtime

```shell
mvn spring-boot:run -Djasypt.encryptor.password=<replace this with the previous password used for encryption>
```

## 6) Testing

You can test endpoints using swagger ui. To do so. run the api then launch your internet browser and navigate
to: [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

## 7) Data structure

![Database Entity Relationship Diagram](src/main/resources/static/PayMyBuddy_data_sturcture_diagram.png?raw=true 'EER diagram.png')

## 8) Api Structure

![Api structure](src/main/resources/static/paymybuddy_diagrammeUML.png?raw=true 'paymybuddy_diagrammeUML.png')

## 9) Run the front-end web interface

[To run the front-end gui clone this repo](https://gitlab.com/abdoum/p06-frontend)
