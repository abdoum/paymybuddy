package com.openclassrooms.paymybuddy;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@EnableEncryptableProperties
public class PaymybuddyApplication {

	@Value("${server.port}")
	private int serverPort;

	public static void main(String[] args) {
		SpringApplication.run(PaymybuddyApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onAppStarted() {
		System.out.println(String.format("Application started on : http://localhost:" + this.serverPort));
	}
}
