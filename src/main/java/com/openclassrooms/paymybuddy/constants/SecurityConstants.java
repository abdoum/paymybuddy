package com.openclassrooms.paymybuddy.constants;

import lombok.Generated;

@Generated
public class SecurityConstants {

	public static final long EXPIRATION_TIME = 15 * 60 * 1000;

	public static final String TOKEN_PREFIX = "Bearer ";

	public static final String JWT_TOKEN_HEADER = "Jwt-Token";

	public static final String TOKEN_VERIFICATION_ERROR = "Token cannot be verified";

	public static final String PAY_MY_BUDDY_LLC = "Pay My Buddy, LLC";

	public static final String PAY_MY_BUDDY_ADMINISTRATION = "User Management Portal";

	public static final String AUTHORITIES = "authorities";

	public static final String FORBIDDEN_MESSAGE = "Please login to access this page";

	public static final String ACCESS_DENIED_MESSAGE = "Access denied. You do not have privileges to access this " +
			"resource";

	public static final String OPTION_HTTP_METHOD = "OPTIONS";

	public static final String[] PUBLIC_URLS = {
			"/login", "register", "/reset-password/**", "/user/image" +
			"/**"};

	public static final String USER_NOT_FOUND_MSG = "user with email %s not found";
}
