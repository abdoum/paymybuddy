package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.services.IAccountService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Email;
import java.util.Optional;

@Generated
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
@Slf4j
public class AccountController {

	@Autowired
	IAccountService iAccountService;

	@GetMapping("/account")
	Account getAccount(@RequestParam("email") @Email String email) throws BadArgument, EmptySearchResult {
		log.info(email.toString());
		Optional<Account> account = iAccountService.findOneByEmail(email);
		var foundAccount = account.orElseThrow(() -> new EmptySearchResult("no account found for username : " + email));
		log.info(foundAccount.toString());
		return foundAccount;
	}
}
