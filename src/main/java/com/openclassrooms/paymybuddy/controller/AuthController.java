package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.JwtResponse;
import com.openclassrooms.paymybuddy.model.LoginRequest;
import com.openclassrooms.paymybuddy.model.SignUpRequest;
import com.openclassrooms.paymybuddy.services.IAccountService;
import com.openclassrooms.paymybuddy.services.IAuthService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Generated
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class AuthController {

	@Autowired
	IAccountService iAccountService;

	@Autowired
	IAuthService iAuthService;

	@PostMapping("/login")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws
			EmptySearchResult {
		log.info(String.valueOf(loginRequest));
		JwtResponse authResult = iAuthService.authenticate(loginRequest.getUsername(), loginRequest.getPassword());
		log.info(authResult.toString());
		return ResponseEntity.ok(authResult);
	}

	@PostMapping("/register")
	public ResponseEntity<JwtResponse> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) throws
			BadArgument, EmptySearchResult {
		log.info(signUpRequest.toString());
		var savedAccount = iAccountService.initializeServices(signUpRequest);
		log.info(savedAccount.toString());
		return ResponseEntity.ok(savedAccount);
	}
}
