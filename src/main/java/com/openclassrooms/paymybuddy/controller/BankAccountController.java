package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.BankAccount;
import com.openclassrooms.paymybuddy.services.IBankAccountService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class BankAccountController {

	@Autowired
	private IBankAccountService iBankAccountService;

	@GetMapping("bank-account/{userId}")
	public BankAccount getUser(@PathVariable("userId") @Min(1) Long userId) throws EmptySearchResult {
		log.info(userId.toString());
		var foundBankAccount = iBankAccountService.getOneByUserId(userId);
		log.info(foundBankAccount.toString());
		return foundBankAccount;
	}
}
