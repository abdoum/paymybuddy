package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Connection;
import com.openclassrooms.paymybuddy.services.IConnectionService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import java.util.Set;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class ConnectionController {

	@Autowired
	IConnectionService iConnectionService;

	@GetMapping("/connection/{id}")
	public Set<Connection> getById(@PathVariable @Min(1) Long id) throws
			EmptySearchResult {
		log.info(id.toString());
		Set<Connection> connections = iConnectionService.getAllConnectionsForUser(id);
		if (connections.isEmpty()) {
			throw new EmptySearchResult("no connections found for user.");
		} else {
			log.info(connections.toString());
			return connections;
		}
	}

	@PostMapping("/connection/{sourceUserId}")
	public Connection add(@RequestBody @Email String targetAccountEmail,
						  @PathVariable @Min(1) Long sourceUserId) throws
			BadArgument {
		log.info(sourceUserId.toString());
		log.info(targetAccountEmail);
		Connection connection = iConnectionService.add(sourceUserId, targetAccountEmail);
		log.info(connection.toString());
		return connection;
	}
}
