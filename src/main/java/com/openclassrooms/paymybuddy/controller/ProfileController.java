package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.services.IUserService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class ProfileController {

	@Autowired
	private IUserService iUserService;

	@GetMapping("profile/{userId}")
	public User getUser(@PathVariable("userId") @Min(1) Long userId) throws EmptySearchResult {
		log.info(userId.toString());
		var foundPerson = iUserService.findOneById(userId);
		if (foundPerson.isEmpty()) {
			throw new EmptySearchResult("No user with id: " + userId);
		}
		log.info(foundPerson.toString());
		return foundPerson.get();
	}
}
