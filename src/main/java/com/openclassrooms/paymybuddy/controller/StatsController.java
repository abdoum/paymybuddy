package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.model.DashboardStats;
import com.openclassrooms.paymybuddy.services.IConnectionService;
import com.openclassrooms.paymybuddy.services.ITransactionService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class StatsController {

	@Autowired
	ITransactionService iTransactionService;

	@Autowired
	IConnectionService iConnectionService;

	@GetMapping("stats/{userId}")
	public ResponseEntity<DashboardStats> getDashboardStats(@PathVariable("userId") @Min(1) Long userId) {
		log.info(userId.toString());
		Double sentTransactions = iTransactionService.getLastMonthSentAmountBySenderWalletId(userId);
		Double receivedTransactions = iTransactionService.getLastMonthReceivedAmountBySenderWalletId(userId);
		Double bankAccountTransactions = iTransactionService.getLastMonthBankTransferredAmountBySenderWalletId(userId);
		Double connectionsCount = iConnectionService.getConnectionsCountBySourceUserId(userId);

		var responseBody = new DashboardStats();
		responseBody.setSentTransactions(sentTransactions);
		responseBody.setReceivedTransactions(receivedTransactions);
		responseBody.setBankAccountTransactions(bankAccountTransactions);
		responseBody.setConnectionsCount(connectionsCount);

		log.info(responseBody.toString());

		return new ResponseEntity<>(
				responseBody,
				HttpStatus.OK);
	}
}
