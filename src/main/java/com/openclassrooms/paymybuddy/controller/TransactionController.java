package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Transaction;
import com.openclassrooms.paymybuddy.model.TransactionRequest;
import com.openclassrooms.paymybuddy.services.ITransactionService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Set;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class TransactionController {

	@Autowired
	ITransactionService iTransactionService;

	@GetMapping("transactions/{userId}")
	public Set<Transaction> getAll(@PathVariable("userId") @Min(1) Long userId) throws EmptySearchResult, BadArgument {
		log.info(userId.toString());
		Set<Transaction> foundTransactions = iTransactionService.getAllTransactionsForAccount(userId);
		if (foundTransactions.isEmpty()) {
			throw new EmptySearchResult("No transactions yet");
		}
		log.info(foundTransactions.toString());
		return foundTransactions;
	}

	@PostMapping("transactions")
	public Transaction add(@Valid @RequestBody TransactionRequest transactionRequest) throws
			BadArgument,
			EmptySearchResult {
		log.info(transactionRequest.toString());
		Transaction addedTransaction = iTransactionService.save(transactionRequest);
		log.info(addedTransaction.toString());
		return addedTransaction;
	}
}
