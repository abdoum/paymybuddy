package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.services.UserService;
import lombok.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Generated
@RestController
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("/user/{idOfUser}")
	Optional<User> search(@Valid @PathVariable String idOfUser) {
		return userService.findOneById((long) Integer.parseInt(idOfUser));
	}
}
