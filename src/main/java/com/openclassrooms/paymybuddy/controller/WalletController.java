package com.openclassrooms.paymybuddy.controller;

import com.openclassrooms.paymybuddy.model.Wallet;
import com.openclassrooms.paymybuddy.services.IWalletService;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

@Generated
@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@Validated
public class WalletController {

	@Autowired
	IWalletService iWalletService;

	@GetMapping("/wallet")
	public Wallet getOneByUserId(@RequestParam("userId") @Min(1) Long userId) {
		log.info(userId.toString());
		var foundWallet = iWalletService.getOneByUserId(userId);
		log.info(foundWallet.toString());
		return foundWallet;
	}
}
