package com.openclassrooms.paymybuddy.exception;

import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@Generated
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadArgument extends Exception {

	public BadArgument(String message) {
		super(message);
		log.error(message);
	}
}
