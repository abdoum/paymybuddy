package com.openclassrooms.paymybuddy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collection;
import java.util.Collections;

@Table(name = "account")
@Entity
@Getter
@Setter
@Generated
@DynamicUpdate
public class Account implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id", nullable = false)
	private Long id;

	@Column(name = "email", nullable = false, length = 50)
	@Email
	@JsonIgnore
	private String email;

	@Column(name = "password", nullable = false, length = 150)
	@Nullable
	@Length(min = 6, max = 150)
	@JsonIgnore
	private String password;

	@Column(name = "credential_expired")
	@JsonIgnore
	private Boolean credentialExpired = false;

	@Column(name = "account_locked")
	@JsonIgnore
	private Boolean accountLocked = false;

	@Column(name = "account_expired")
	@JsonIgnore
	private Boolean accountExpired = false;

	@Column(name = "account_enabled")
	@JsonIgnore
	private Boolean accountEnabled = true;

	@Enumerated(EnumType.STRING)
	@JsonIgnore
	private Roles roles = Roles.USER;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SimpleGrantedAuthority authority =
				new SimpleGrantedAuthority(roles.name());
		return Collections.singletonList(authority);
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	@JsonIgnore
	public String getUsername() {
		return email;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return !accountExpired;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return !accountLocked;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return !credentialExpired;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return accountEnabled;
	}

	@Override
	public String toString() {
		return "Account{" +
				"id=" + id +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", roles='" + roles + '\'' +
				", credentialExpired=" + credentialExpired +
				", accountLocked=" + accountLocked +
				", accountExpired=" + accountExpired +
				", accountEnabled=" + accountEnabled +
				'}';
	}
}
