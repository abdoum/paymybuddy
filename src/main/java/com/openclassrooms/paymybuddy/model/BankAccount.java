package com.openclassrooms.paymybuddy.model;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "bank_account")
@DynamicUpdate
@Generated
public class BankAccount {

	@Id
	@Column(name = "bank_account_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "iban", nullable = false, length = 37)
	private String iban;

	@Column(name = "swift_code", nullable = false, length = 11)
	private String swiftCode;

	@Column(name = "owner_full_name", nullable = false, length = 100)
	private String ownerFullName;

	@OneToOne(cascade = CascadeType.REMOVE, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Override
	public String toString() {
		return "BankAccount{" +
				"bankAccountId=" + id +
				", iban='" + iban + '\'' +
				", swiftCode='" + swiftCode + '\'' +
				", ownerFullName='" + ownerFullName + '\'' +
				'}';
	}
}
