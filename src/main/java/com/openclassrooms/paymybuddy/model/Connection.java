package com.openclassrooms.paymybuddy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Generated;

import javax.persistence.*;

@Table(name = "connection")
@Entity
@Data
@Generated
public class Connection {

	@Id
	@Column(name = "connection_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "source_user_id", referencedColumnName = "user_id")
	private User sourceUser;

	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "target_user_id", referencedColumnName = "user_id")
	private User targetUser;
}
