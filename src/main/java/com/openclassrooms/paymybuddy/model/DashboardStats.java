package com.openclassrooms.paymybuddy.model;

import lombok.Generated;

@Generated
public class DashboardStats {

	private Double sentTransactions;

	private Double receivedTransactions;

	private Double bankAccountTransactions;

	private Double connectionsCount;

	public Double getConnectionsCount() {
		return connectionsCount;
	}

	public void setConnectionsCount(Double connectionsCount) {
		this.connectionsCount = connectionsCount;
	}

	public Double getSentTransactions() {
		return sentTransactions;
	}

	public void setSentTransactions(Double sentTransactions) {
		this.sentTransactions = sentTransactions;
	}

	public Double getReceivedTransactions() {
		return receivedTransactions;
	}

	public void setReceivedTransactions(Double receivedTransactions) {
		this.receivedTransactions = receivedTransactions;
	}

	public Double getBankAccountTransactions() {
		return bankAccountTransactions;
	}

	public void setBankAccountTransactions(Double bankAccountTransactions) {
		this.bankAccountTransactions = bankAccountTransactions;
	}

	@Override
	public String toString() {
		return "DashboardStats{" +
				"sentTransactions=" + sentTransactions +
				", receivedTransactions=" + receivedTransactions +
				", bankAccountTransactions=" + bankAccountTransactions +
				'}';
	}
}
