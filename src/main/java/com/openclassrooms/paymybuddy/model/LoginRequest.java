package com.openclassrooms.paymybuddy.model;

import lombok.Generated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Generated
public class LoginRequest {

	@NotBlank
	@Email
	private String username;

	@NotBlank
	@Pattern(regexp = "[\\p{Alnum}\\-.?@]{6,25}")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginRequest{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				'}';
	}
}
