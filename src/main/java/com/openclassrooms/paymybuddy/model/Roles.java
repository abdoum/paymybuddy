package com.openclassrooms.paymybuddy.model;

import lombok.Generated;

@Generated
public enum Roles {
	USER,
	ADMIN
}
