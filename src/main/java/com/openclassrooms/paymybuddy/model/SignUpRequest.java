package com.openclassrooms.paymybuddy.model;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Generated
public class SignUpRequest {

	@Email
	@NotBlank
	private String username;

	@NotBlank
	@Size(min = 6, max = 25)
	private String password;

	@NotBlank
	@Size(min = 6, max = 25)
	private String passwordConfirmation;

	@NotBlank
	@Size(min = 2, max = 15)
	private String firstName;

	@NotBlank
	@Size(min = 2, max = 15)
	private String lastName;

	@NotNull
	private BankAccount bankAccount;

	@Override
	public String toString() {
		return "SignUpRequest{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				", passwordConfirmation='" + passwordConfirmation + '\'' +
				'}';
	}
}
