package com.openclassrooms.paymybuddy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.Optional;

@Table(name = "transaction")
@Getter
@Setter
@Entity
@Generated
@DynamicUpdate
public class Transaction {

	@Id
	@Column(name = "transaction_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(cascade = CascadeType.PERSIST, optional = true, orphanRemoval = true)
	@JoinColumn(name = "sender_wallet_id", nullable = true)
	private Wallet senderWallet;

	@OneToOne(cascade = CascadeType.PERSIST, optional = true, orphanRemoval = true)
	@JoinColumn(name = "receiver_wallet_id", nullable = false)
	@Nullable
	private Wallet receiverWallet;

	@FutureOrPresent
	@Column(name = "date", nullable = false)
	private LocalDate date = LocalDate.now();

	@Column(name = "description")
	@Nullable
	private String description;

	@Positive
	@Column(name = "amount", nullable = false)
	private Double amount;

	@Positive
	@JsonIgnore
	@Column(name = "commission_amount", nullable = false)
	private Double commissionAmount;

	@OneToOne(cascade = CascadeType.PERSIST, optional = true)
	@JoinColumn(name = "receiver_bank_account_id", nullable = true)
	@Nullable
	private BankAccount receiverBankAccount;

	public void setAmount(@Positive Double amount) {
		this.amount = amount;
		this.commissionAmount = amount != null ? amount * 0.005 : 0;
	}

	@Override
	public String toString() {
		Optional<String> receiverBalance = Optional.ofNullable(
				this.receiverWallet != null ? this.receiverWallet.getBalance().toString() : null);
		Optional<String> senderWalletBalance = Optional.of(this.senderWallet != null ?
				this.senderWallet.getBalance().toString() : "");
		return "Transaction{" +
				"id=" + id +
				", senderWallet=" + senderWalletBalance.orElse("none") +
				", receiverBalanceOrBankAccountId=" + receiverBalance.orElse(receiverBankAccount != null ?
				receiverBankAccount.getIban() : "none") +
				", date=" + date +
				", description='" + description +
				", amount=" + amount +
				", commissionAmount=" + commissionAmount +
				'}';
	}
}
