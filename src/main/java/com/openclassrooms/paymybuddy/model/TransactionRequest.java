package com.openclassrooms.paymybuddy.model;

import jdk.jfr.BooleanFlag;
import lombok.Data;
import lombok.Generated;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Data
@Generated
public class TransactionRequest {

	@Min(1)
	private Long senderUserId;

	@Min(1)
	private Long receiverUserId;

	@NumberFormat(style = NumberFormat.Style.CURRENCY)
	private Double amount;

	@BooleanFlag
	private Boolean isWalletRecharge = false;

	@Nullable
	@Length(max = 250)
	@Pattern(regexp = "[\\p{Alpha}\\p{Space}\\p{Alnum}-:.]{0,250}")
	private String description;
}
