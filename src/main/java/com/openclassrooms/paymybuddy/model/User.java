package com.openclassrooms.paymybuddy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

import static org.apache.commons.lang.WordUtils.capitalizeFully;

@Table(name = "user")
@Entity
@Getter
@Setter
@Generated
@DynamicUpdate
public class User {

	@JsonIgnore
	@OneToOne(cascade =
			CascadeType.ALL
			, optional = false, orphanRemoval = true)
	@JoinColumn(name = "account_id", nullable = false)
	Account account;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", nullable = false)
	private Long id;

	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName;

	@Column(name = "last_name", nullable = false, length = 50)
	private String lastName;

	public void setFirstName(String firstName) {
		this.firstName = StringUtils.normalizeSpace(capitalizeFully(firstName));
	}

	public void setLastName(String lastName) {
		this.lastName = StringUtils.normalizeSpace(capitalizeFully(lastName));
	}

	@Override
	public String toString() {
		return "User{" +
				"account=" + account +
				", id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				'}';
	}
}
