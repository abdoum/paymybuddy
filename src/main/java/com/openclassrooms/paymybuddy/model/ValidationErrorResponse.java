package com.openclassrooms.paymybuddy.model;

import lombok.Data;
import lombok.Generated;

import java.util.ArrayList;
import java.util.List;

@Data
@Generated
public class ValidationErrorResponse {

	private List<Violation> violations = new ArrayList<>();
}
