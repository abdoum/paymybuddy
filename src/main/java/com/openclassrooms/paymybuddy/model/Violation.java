package com.openclassrooms.paymybuddy.model;

import lombok.Data;
import lombok.Generated;

@Data
@Generated
public class Violation {

	private String fieldName;

	private String message;

	public Violation(String fieldName, String message) {
		this.fieldName = fieldName;
		this.message = message;
	}
}
