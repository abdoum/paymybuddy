package com.openclassrooms.paymybuddy.model;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.util.Currency;
import java.util.Locale;

@Table(name = "wallet")
@Entity
@Getter
@Setter
@Generated
@DynamicUpdate
public class Wallet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "wallet_id", nullable = false)
	private Long id;

	@Column(name = "currency", nullable = false, length = 3)
	private Currency currency = Currency.getInstance(Locale.FRANCE);

	@Column(name = "balance", nullable = false)
	@NumberFormat
	private Double balance = 0.d;

	@OneToOne(cascade = CascadeType.REMOVE, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Override
	public String toString() {
		return "Wallet{" +
				"id=" + id +
				", currency=" + currency +
				", balance=" + balance +
				'}';
	}
}
