package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Account;
import lombok.Generated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Generated
public interface IAccountDAO extends JpaRepository<Account, Long> {

	Optional<Account> findByEmail(String email);

	Optional<Account> getByEmail(String email);
}
