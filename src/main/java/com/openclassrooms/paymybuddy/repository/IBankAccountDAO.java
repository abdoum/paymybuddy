package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.BankAccount;
import lombok.Generated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Generated
public interface IBankAccountDAO extends JpaRepository<BankAccount, Long> {

	Optional<BankAccount> findByUserId(Long receiverUserId);
}
