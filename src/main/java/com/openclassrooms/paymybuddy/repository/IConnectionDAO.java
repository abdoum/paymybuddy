package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Connection;
import com.openclassrooms.paymybuddy.model.User;
import lombok.Generated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
@Generated
public interface IConnectionDAO extends JpaRepository<Connection, Long> {

	Set<Connection> findBySourceUser(User sourceUser);

	Optional<Connection> getBySourceUser(User sourceUser);

	@Query(
			value = "select * from #{#entityName} where source_user_id = :sourceUserId and target_user_id " +
					"\n" +
					"= (SELECT account_id from account where email = :targetUserEmail )",
			nativeQuery = true)
	Optional<Connection> findOneBySourceUserIdAndTargetUser(@Param("sourceUserId") Long sourceUserId,
															@Param("targetUserEmail") String targetUserEmail);

	@Query(
			value = "select COUNT(connection_id) from #{#entityName} where source_user_id = :sourceUserId",
			nativeQuery = true)
	Optional<Double> getConnectionsCountBySourceUserId(Long sourceUserId);
}
