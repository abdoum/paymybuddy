package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Transaction;
import com.openclassrooms.paymybuddy.model.Wallet;
import lombok.Generated;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
@Generated
public interface ITransactionDAO extends JpaRepository<Transaction, Long> {

	Set<Transaction> findAllBySenderWallet(Wallet wallet, Sort date);

	@Query(
			value = "SELECT SUM(amount) FROM #{#entityName} \n" +
					"WHERE date BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW() AND (sender_wallet_id = " +
					":senderWalletId);",
			nativeQuery = true)
	Optional<Double> getLastMonthSentAmountBySenderWalletId(@Param("senderWalletId") Long senderWalletId);

	@Query(
			value = "SELECT SUM(amount) FROM #{#entityName} \n" +
					"WHERE date BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW() AND (receiver_wallet_id = " +
					":receiverWalletId);",
			nativeQuery = true)
	Optional<Double> getLastMonthReceivedAmountBySenderWalletId(@Param("receiverWalletId") Long receiverWalletId);

	@Query(
			value = "SELECT SUM(amount) FROM #{#entityName} \n" +
					"WHERE date BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW() AND (receiver_bank_account_id = " +
					":receiverBankAccountId);",
			nativeQuery = true)
	Optional<Double> getLastMonthBankTransferredAmountBySenderWalletId(
			@Param("receiverBankAccountId") Long receiverBankAccountId);
}
