package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Wallet;
import lombok.Generated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Generated
public interface IWalletDAO extends JpaRepository<Wallet, Long> {

	Optional<Wallet> findByUserId(Long userId);

	Wallet getOneByUserId(Long userId);
}
