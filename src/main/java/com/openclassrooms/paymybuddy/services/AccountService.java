package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.*;
import com.openclassrooms.paymybuddy.repository.IAccountDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

import static com.openclassrooms.paymybuddy.constants.SecurityConstants.USER_NOT_FOUND_MSG;

@Service
@Slf4j
public class AccountService implements IAccountService {

	@Autowired
	IAccountDAO iAccountDAO;

	@Autowired
	IAuthService iAuthService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	IUserService iUserService;

	@Autowired
	IWalletService iWalletService;

	@Autowired
	IBankAccountService iBankAccountService;

	/**
	 * Save account.
	 *
	 * @param signUpRequest the sign-up request
	 * @return the account
	 * @throws BadArgument the bad argument
	 */
	@Override
	public Account save(@NotNull SignUpRequest signUpRequest) throws BadArgument {
		var accountToPersist = new Account();
		accountToPersist.setEmail(signUpRequest.getUsername());
		accountToPersist.setPassword(signUpRequest.getPassword());
		var existingEmail = findOneByEmail(signUpRequest.getUsername());

		if (existingEmail.isPresent()) {
			throw new BadArgument("Error: Email already " +
					"in use!");
		}
		Account account = new Account();
		account.setEmail(signUpRequest.getUsername());
		account.setPassword(
				signUpRequest.getPassword());
		account.setRoles(Roles.USER);
		var encryptedPassword = passwordEncoder.encode(account.getPassword());
		account.setPassword(encryptedPassword);
		return iAccountDAO.save(account);
	}

	/**
	 * Save account to database.
	 *
	 * @param account the account
	 * @return the saved account
	 * @throws BadArgument the bad argument
	 */
	@Override
	public Account save(@NotNull Account account) throws BadArgument {
		return iAccountDAO.save(account);
	}

	/**
	 * Gets Account by email.
	 *
	 * @param email the email
	 * @return the by email
	 */
	@Override
	public Optional<Account> getByEmail(@Email String email) {
		return iAccountDAO.getByEmail(email);
	}

	/**
	 * Load account by username.
	 *
	 * @param username the username
	 * @return the account
	 * @throws UsernameNotFoundException the username not found exception
	 */
	@Override
	public Account loadUserByUsername(String username) throws UsernameNotFoundException {
		var account = getByEmail(username);
		return account.orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, username)));
	}

	/**
	 * Find a single account by email.
	 *
	 * @param email the email
	 * @return the account
	 * @throws BadArgument the bad argument
	 */
	@Override
	public Optional<Account> findOneByEmail(@NotBlank @Email String email) throws BadArgument {
		return iAccountDAO.findByEmail(email.replaceAll("\\h+", ""));
	}

	/**
	 * Initialize service account.
	 *
	 * @param signUpRequest the sign up request
	 * @return the account
	 * @throws BadArgument the bad argument
	 */
	@Override
	public JwtResponse initializeServices(@NotNull SignUpRequest signUpRequest) throws BadArgument, EmptySearchResult {
		var newAccount = save(signUpRequest);
		var newUser = iUserService.save(signUpRequest, newAccount);
		iWalletService.save(newUser);
		log.debug(signUpRequest.getBankAccount().toString());

		var bankAccount = new BankAccount();
		bankAccount.setIban(signUpRequest.getBankAccount().getIban());
		bankAccount.setSwiftCode(signUpRequest.getBankAccount().getSwiftCode());
		bankAccount.setOwnerFullName(signUpRequest.getBankAccount().getOwnerFullName());
		bankAccount.setUser(newUser);
		iBankAccountService.save(bankAccount);
		log.debug(bankAccount.toString());

		return iAuthService.authenticate(signUpRequest.getUsername(), signUpRequest.getPassword());
	}
	// todo explain tech choices briefly (angular,  primeng, global architecture)
	//  talk about config choices for auth and security
	//  make a live demo
	// 	add unit test for a 60% coverage at least
}
