package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.config.jwt.JwtUtils;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collection;

@Service
public class AuthService implements IAuthService {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	IUserService iUserService;

	/**
	 * Authenticate user.
	 *
	 * @param username the username
	 * @param password the password
	 * @return the jwt response
	 * @throws EmptySearchResult the empty search result
	 */
	@Override
	public JwtResponse authenticate(@Email String username, @NotBlank String password) throws EmptySearchResult {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(username, password));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		Account userDetails = (Account) authentication.getPrincipal();
		var user = iUserService.findOneByAccount(userDetails).orElseThrow(() -> new EmptySearchResult("ser not " +
				"Found for account"));
		Collection<? extends GrantedAuthority> roles = userDetails.getAuthorities();
		return new JwtResponse(jwt,
				user.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles);
	}
}
