package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.BankAccount;
import com.openclassrooms.paymybuddy.repository.IBankAccountDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Service
public class BankAccountService implements IBankAccountService {

	@Autowired
	IBankAccountDAO iBankAccountDAO;

	/**
	 * Save bank account.
	 *
	 * @param bankAccount the bank account
	 * @return the saved bank account
	 * @throws BadArgument the bad argument
	 */
	@Override
	public BankAccount save(@NotNull BankAccount bankAccount) {
		return iBankAccountDAO.save(bankAccount);
	}

	/**
	 * Gets Bank Account by user id.
	 *
	 * @param receiverUserId the receiver user id
	 * @return the found bank account
	 * @throws EmptySearchResult the empty search result
	 */
	@Override
	public BankAccount getOneByUserId(@Positive Long receiverUserId) throws EmptySearchResult {
		return iBankAccountDAO.findByUserId(receiverUserId)
				.orElseThrow(() -> new EmptySearchResult("An error occurred while trying to add " +
						"the bank " +
						"account"));
	}
}
