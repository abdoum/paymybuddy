package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Connection;
import com.openclassrooms.paymybuddy.repository.IConnectionDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Positive;
import java.util.Set;

@Service
@Slf4j
public class ConnectionService implements IConnectionService {

    @Autowired
    IUserService iUserService;

    @Autowired
    IConnectionDAO iConnectionDAO;

    @Autowired
    IAccountService iAccountService;

    /**
     * Gets all connections for user.
     *
     * @param id the source user id
     * @return all found connections for the user
     * @throws EmptySearchResult empty search result
     */
    @Override
    public Set<Connection> getAllConnectionsForUser(@Positive Long id) throws EmptySearchResult {
        var sourceUser = iUserService.findOneById(id).orElseThrow(() -> new EmptySearchResult("no user found."));
        return iConnectionDAO.findBySourceUser(sourceUser);
    }

    /**
     * Saves a connection.
     *
     * @param sourceUserId    the source user id
     * @param targetUserEmail the target user email
     * @return the saved connection
     * @throws BadArgument bad argument
     */
    @Override
    public Connection add(@Positive Long sourceUserId, @Positive String targetUserEmail) throws BadArgument {
        var connectionToPersist = new Connection();
        var foundSourceUser = iUserService.findOneById(sourceUserId).orElseThrow(() -> new BadArgument("Source user not found"));
        var existingConnection = iConnectionDAO.findOneBySourceUserIdAndTargetUser(sourceUserId, targetUserEmail);
        if (existingConnection.isPresent()) {
            throw new BadArgument("Connection already added for : " + targetUserEmail);
        }

        var foundTargetAccount = iAccountService.findOneByEmail(targetUserEmail).orElseThrow(() -> new BadArgument(
                "Account" +
                        " not found"));

        var targetUser =
                iUserService.findOneByAccount(foundTargetAccount)
                        .orElseThrow(() -> new BadArgument("No user found for : " + targetUserEmail));
        if (sourceUserId.equals(targetUser.getId())) {
            throw new BadArgument("Please add another account as a connection");
        }
        connectionToPersist.setSourceUser(foundSourceUser);
        connectionToPersist.setTargetUser(targetUser);

        return iConnectionDAO.save(connectionToPersist);
    }

    /**
     * Gets connections count by source user id.
     *
     * @param userId the source user id
     * @return the connections count or 0 if no connections were found
     */
    @Override
    public Double getConnectionsCountBySourceUserId(@Positive Long userId) {
        return iConnectionDAO.getConnectionsCountBySourceUserId(userId).orElse(0D);
    }
}
