package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.JwtResponse;
import com.openclassrooms.paymybuddy.model.SignUpRequest;
import lombok.Generated;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@Generated
public interface IAccountService extends UserDetailsService {

	Account save(SignUpRequest signUpRequest) throws BadArgument;

	Account save(Account account) throws BadArgument;

	Optional<Account> getByEmail(String email);

	@Override
	Account loadUserByUsername(String username) throws UsernameNotFoundException;

	Optional<Account> findOneByEmail(String email) throws BadArgument;

	JwtResponse initializeServices(SignUpRequest signUpRequest) throws BadArgument, EmptySearchResult;
}
