package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.JwtResponse;
import lombok.Generated;

@Generated
public interface IAuthService {

	JwtResponse authenticate(String username, String password) throws EmptySearchResult;
}
