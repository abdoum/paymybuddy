package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.BankAccount;
import lombok.Generated;

@Generated
public interface IBankAccountService {

	BankAccount save(BankAccount bankAccount);

	BankAccount getOneByUserId(Long receiverUserId) throws EmptySearchResult;
}
