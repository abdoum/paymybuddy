package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Connection;
import lombok.Generated;

import java.util.Set;

@Generated
public interface IConnectionService {

	Set<Connection> getAllConnectionsForUser(Long id) throws EmptySearchResult;

	Connection add(Long sourceUserId, String targetUserEmail) throws BadArgument;

	Double getConnectionsCountBySourceUserId(Long userId);
}
