package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Transaction;
import com.openclassrooms.paymybuddy.model.TransactionRequest;
import lombok.Generated;

import javax.transaction.Transactional;
import java.util.Set;

@Generated
public interface ITransactionService {

	@Transactional
	Transaction save(TransactionRequest transaction) throws BadArgument, EmptySearchResult;

	Set<Transaction> getAllTransactionsForAccount(Long userId) throws EmptySearchResult, BadArgument;

	@Transactional
	Transaction rechargeWallet(TransactionRequest transactionRequest) throws BadArgument, EmptySearchResult;

	Double getLastMonthSentAmountBySenderWalletId(Long senderWalletId);

	Double getLastMonthReceivedAmountBySenderWalletId(Long receiverWalletId);

	Double getLastMonthBankTransferredAmountBySenderWalletId(Long receiverBankAccountId);
}
