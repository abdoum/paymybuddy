package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.SignUpRequest;
import com.openclassrooms.paymybuddy.model.User;
import lombok.Generated;

import java.util.Optional;

@Generated
public interface IUserService {

	User save(SignUpRequest signUpRequest, Account account);

	Optional<User> findOneById(Long id);

	Optional<User> findOneByAccount(Account account);
}
