package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.model.Wallet;
import lombok.Generated;

import java.util.Optional;

@Generated
public interface IWalletService {

	Wallet save(User user);

	Optional<Wallet> findWalletByUserId(Long userId);

	Wallet getOneByUserId(Long userId);
}
