package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Transaction;
import com.openclassrooms.paymybuddy.model.TransactionRequest;
import com.openclassrooms.paymybuddy.repository.ITransactionDAO;
import com.openclassrooms.paymybuddy.repository.IWalletDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

@Service
@Slf4j
public class TransactionService implements ITransactionService {

    @Autowired
    ITransactionDAO iTransactionDAO;

    @Autowired
    IWalletDAO iWalletDAO;

    @Autowired
    IWalletService iWalletService;

    @Autowired
    IBankAccountService iBankAccountService;

    /**
     * Saves a transaction. If the sender is the same as the receiver then the transaction is sent to the sender bank
     * account instead of his wallet.
     *
     * @param transaction the transaction request
     * @return the saved transaction
     * @throws BadArgument       bad argument
     * @throws EmptySearchResult empty search result
     * @see TransactionRequest
     */
    @Override
    @Transactional
    public Transaction save(@NotNull TransactionRequest transaction) throws BadArgument, EmptySearchResult {
        if (Boolean.TRUE.equals(transaction.getIsWalletRecharge())) {
            return rechargeWallet(transaction);
        }
        var senderWallet = iWalletService.getOneByUserId(transaction.getSenderUserId());
        var transactionToPersist = new Transaction();
        var receiverWallet = iWalletService.getOneByUserId(transaction.getReceiverUserId());

        if (senderWallet.getBalance() < transaction.getAmount()) {
            throw new BadArgument(
                    "Your balance does not cover the transfer amount. The maximum allowed amount is: " +
                            senderWallet.getBalance());
        }

        if (!transaction.getSenderUserId().equals(transaction.getReceiverUserId())) {
            transactionToPersist.setReceiverWallet(receiverWallet);
            receiverWallet.setBalance(receiverWallet.getBalance() + transaction.getAmount());
            iWalletDAO.save(receiverWallet);
        }

        senderWallet.setBalance(senderWallet.getBalance() - transaction.getAmount());
        iWalletDAO.save(senderWallet);

        transactionToPersist.setSenderWallet(senderWallet);
        transactionToPersist.setAmount(transaction.getAmount());
        transactionToPersist.setDescription(transaction.getDescription());

        var receiverBankAccount = iBankAccountService.getOneByUserId(transaction.getReceiverUserId());

        if (transaction.getSenderUserId().equals(transaction.getReceiverUserId())) {
            transactionToPersist.setReceiverBankAccount(receiverBankAccount);
        }

        return iTransactionDAO.save(transactionToPersist);
    }

    /**
     * Gets all transactions for account.
     *
     * @param userId the user id
     * @return the all transactions for account
     * @throws EmptySearchResult the empty search result
     */
    @Override
    public Set<Transaction> getAllTransactionsForAccount(@Positive Long userId) throws EmptySearchResult, BadArgument {
        var foundWallet = iWalletService.findWalletByUserId(userId)
                .orElseThrow(() -> new BadArgument("No wallet found for user with id :"
                        + userId));
        var foundTransactions = iTransactionDAO.findAllBySenderWallet(foundWallet, Sort.by(Sort.Direction.DESC,
                "date"));
        if (foundTransactions.isEmpty()) {
            throw new EmptySearchResult("No transactions found for wallet " + foundWallet.getId());
        }
        return foundTransactions;
    }

    /**
     * Recharge wallet balance.
     *
     * @param transactionRequest the transaction request
     * @return the saved transaction
     * @throws EmptySearchResult if empty search result
     */
    @Override
    public Transaction rechargeWallet(@NotNull TransactionRequest transactionRequest) throws EmptySearchResult {
        var transactionToPersist = new Transaction();
        var receiverWallet = iWalletService.getOneByUserId(transactionRequest.getReceiverUserId());

        transactionToPersist.setReceiverWallet(receiverWallet);
        receiverWallet.setBalance(receiverWallet.getBalance() + transactionRequest.getAmount());
        iWalletDAO.save(receiverWallet);
        transactionToPersist.setAmount(transactionRequest.getAmount());
        transactionToPersist.setDescription(transactionRequest.getDescription());

        var receiverBankAccount = iBankAccountService.getOneByUserId(transactionRequest.getReceiverUserId());
        transactionToPersist.setReceiverBankAccount(receiverBankAccount);
        return iTransactionDAO.save(transactionToPersist);
    }

    /**
     * Gets a sum of last month sent amount by sender wallet id.
     *
     * @param userId the user id
     * @return the last month sent amount by sender wallet id
     */
    @Override
    public Double getLastMonthSentAmountBySenderWalletId(@Positive Long userId) {
        var walletId = iWalletService.getOneByUserId(userId).getId();
        return iTransactionDAO.getLastMonthSentAmountBySenderWalletId(walletId).orElse(0D);
    }

    /**
     * Gets a sum of last month received amount by sender wallet id.
     *
     * @param receiverWalletId the receiver wallet id
     * @return the last month received amount by sender wallet id
     */
    @Override
    public Double getLastMonthReceivedAmountBySenderWalletId(@Positive Long receiverWalletId) {
        var walletId = iWalletService.getOneByUserId(receiverWalletId).getId();
        return iTransactionDAO.getLastMonthReceivedAmountBySenderWalletId(walletId).orElse(0D);
    }

    /**
     * Gets a sum of last month bank transferred amount by sender wallet id.
     *
     * @param receiverBankAccountId the receiver bank account id
     * @return the last month bank transferred amount by sender wallet id
     */
    @Override
    public Double getLastMonthBankTransferredAmountBySenderWalletId(@Positive Long receiverBankAccountId) {
        var walletId = iWalletService.getOneByUserId(receiverBankAccountId).getId();
        return iTransactionDAO.getLastMonthBankTransferredAmountBySenderWalletId(walletId).orElse(0D);
    }
}
