package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.SignUpRequest;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.repository.IUserDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

@Service
@Slf4j
public class UserService implements IUserService {

	@Autowired
	IUserDAO iUserDAO;

	/**
	 * Saves a user to database.
	 *
	 * @param signUpRequest the sign up request
	 * @return the user
	 */
	@Override
	public User save(@NotNull SignUpRequest signUpRequest, Account account) {
		var persistentUser = new User();
		persistentUser.setFirstName(signUpRequest.getFirstName());
		persistentUser.setLastName(signUpRequest.getLastName());
		persistentUser.setAccount(account);
		var savedUser = iUserDAO.save(persistentUser);
		log.debug(savedUser.toString());

		return savedUser;
	}

	/**
	 * Find a user by id.
	 *
	 * @param id the id
	 * @return an optional
	 */
	@Override
	public Optional<User> findOneById(@Positive Long id) {
		return iUserDAO.findById(id);
	}

	/**
	 * Find one by account optional.
	 *
	 * @param account the account
	 * @return the optional
	 */
	@Override
	public Optional<User> findOneByAccount(@NotNull Account account) {
		return iUserDAO.findOneByAccount(account);
	}
}
