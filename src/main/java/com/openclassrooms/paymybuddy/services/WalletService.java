package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.model.Wallet;
import com.openclassrooms.paymybuddy.repository.IWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

@Service
public class WalletService implements IWalletService {

	@Autowired
	IWalletDAO iWalletDAO;

	/**
	 * Creates a wallet and saves it to database.
	 *
	 * @param user the user
	 * @return the wallet
	 */
	@Override
	public Wallet save(@NotNull User user) {
		Wallet walletToPersist = new Wallet();
		walletToPersist.setUser(user);
		return iWalletDAO.save(walletToPersist);
	}

	/**
	 * Find a wallet by user id.
	 *
	 * @param userId the user id
	 * @return optional
	 */
	@Override
	public Optional<Wallet> findWalletByUserId(@Positive Long userId) {
		return iWalletDAO.findByUserId(userId);
	}

	/**
	 * Gets a wallet by user id.
	 *
	 * @param userId the user id
	 * @return the one by user id
	 */
	@Override
	public Wallet getOneByUserId(@Positive Long userId) {
		return iWalletDAO.getOneByUserId(userId);
	}
}
