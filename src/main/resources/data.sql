START transaction;

INSERT INTO `account` (`account_id`, `account_enabled`, `account_expired`, `account_locked`, `credential_expired`, `email`, `password`, `roles`) VALUES
(1, b'1', b'0', b'0', b'0', 'abc@reuters.com', '$2a$10$kOLeidPMRpgEzBU4DzRchOzNq5h6CSIGyZ8jn0XdWTDI3ki7SuK.m', 'USER'),
(2, b'1', b'0', b'0', b'0', 'ptourne1@reuters.com', '$2a$10$GWSnPTedBDQT/KTp9jHtiu3X/MAH/Sg/F3v2JFmTXu/y/68BMC5zC', 'USER'),
(3, b'0', b'0', b'0', b'1', 'jmcgreal2@newyorker.com', '$2a$10$eJ0DgAJdd.WVdifeJRNMneXEWY8LeaMM6EG4zyi7LPQ8JsSSIJu7G', 'USER'),
(4, b'1', b'1', b'1', b'0', 'hhurlin3@yandex.ru', '$2a$10$Sh8123ZobyFioUcvgpgpu.tOmcEEnlG1e/yv6s9jEWfT61lsVwlE.', 'USER'),
(5, b'1', b'1', b'0', b'1', 'sgoldster4@economist.com', '$2a$10$Um5tqAg0ZBzfQg1NWvGXL.nLBXz2IQqElYIw/x22.TozDPtoxWMbC', 'USER'),
(6, b'0', b'1', b'0', b'0', 'rmerrison5@bloglovin.com', '$2a$10$cxHjzI5fRZ5Pm4LpIoq8DOZtyXSctHQE6sBpKQwgdjPa9SSlnqi.O', 'USER'),
(7, b'1', b'1', b'0', b'0', 'cgamlen6@rediff.com', '$2a$10$68pM048XqBjnIHNweB9qTuEkZwpYaYKEuEOJQBFSgMu9jooPYxQ/O', 'USER'),
(8, b'1', b'1', b'0', b'1', 'pkuzemka7@google.nl', '$2a$10$EOawgfruHFOt49BJtLRMeu5f7aFWHcGURJ8OcFi6/9X8UZs28GZrm', 'USER'),
(9, b'1', b'0', b'0', b'0', 'chaack8@wordpress.com', '$2a$10$1qcUngshkbNiJyT2lq75pebY11miPrv8kGb5eYcfmAZsEcEwIQ0Ja', 'USER'),
(10, b'1', b'1', b'1', b'0', 'onerger9@ask.com', '$2a$10$PtEaQgRColFcN3sGbmJ5OOKBaFA941BvlDRjGEv03QWaVpVMzAPz6', 'USER'),
(11, b'0', b'1', b'1', b'0', 'bmccathaya@princeton.edu', '$2a$10$h9YKJFGnc5L5i3iPLGw61OZFFaGDlsgT/kc1af5PKWKX92OCG7QFC', 'USER'),
(12, b'1', b'1', b'1', b'0', 'gparamorb@yandex.ru', '$2a$10$m3SvfOsM25Kr9KbDIdxZxuKksyYoSyMfdn9oOM7.vQlliZCMlScAi', 'USER'),
(13, b'0', b'1', b'1', b'0', 'asandbachc@google.co.jp', '$2a$10$QNA9az5l/n/tlXVUFDy5DuoWegOfFh1iWnM3/JePot9IHzKedxiZ2', 'USER'),
(14, b'0', b'1', b'0', b'1', 'gmetheringhamd@tiny.cc', '$2a$10$v0J98MFytbDDn1cuNevdm.16w2RThJTpYuTNhzOCbXFR1v8q/28km', 'USER'),
(15, b'1', b'1', b'0', b'0', 'tboveye@tmall.com', '$2a$10$W2H7FB2JBuutV6qd9Y6NQ.TneQAjp9lE45iJG3iCnbrruPv9TzjYO', 'USER'),
(16, b'1', b'1', b'1', b'1', 'vbaggsf@e-recht24.de', '$2a$10$TvLSSUL1jojNiWTJKhXq0eoD6G4M9XNkXqU0zCnn4NWIbSLYnDcZS', 'USER'),
(17, b'1', b'1', b'1', b'1', 'swhittlesg@sbwire.com', '$2a$10$0PPFgF2RLDVxzvtCmcM4TuH8sPic9nHAbgOiGIe1KdHqSHjSTDhf2', 'USER'),
(18, b'0', b'1', b'0', b'0', 'ehearleh@scribd.com', '$2a$10$F3WZgkIjfrMUdwJ64TSqwu9dDfvILRYdf17VHRFIE4Qu1QXEP0OVG', 'USER'),
(19, b'1', b'0', b'1', b'0', 'jpeilei@nba.com', '$2a$10$0zgsLShHX8hqFU0nHsxdiOBYJ6U8SYArxpal5WH1CZwsNyeZTIUwO', 'USER'),
(20, b'0', b'1', b'1', b'0', 'skordesj@naver.com', '$2a$10$EYpmAd85DpIk04190RF2b.eENgK2z59w5CSQc/EsYrVHjEN91gAvK', 'USER'),
(21, b'0', b'0', b'0', b'0', 'cblakesleyk@cbslocal.com', '$2a$10$Cbizm26Ux4oBz80ZsnMMYeJNVCuh1LyOs.aZW2F01J7HjJS8AUgFu', 'USER'),
(22, b'1', b'1', b'1', b'0', 'sfraysl@oakley.com', '$2a$10$o3Rszw23cgHJnSkWKUNlruTKD3L9FQ4zePqDQSVf4AZZmWuZWQuuO', 'USER'),
(23, b'0', b'1', b'0', b'1', 'mjanuszewiczm@cam.ac.uk', '$2a$10$QWJiA/iPpCPrsKdeF96nVetFKbtX0lJBOKUEFqJIhS42tMqqQmrw6', 'USER'),
(24, b'0', b'1', b'1', b'0', 'mmorantn@shutterfly.com', '$2a$10$Ib7xVV57OozEKYJ7PzdYOOjcbcosXwXPyhR7UWCXDKJxR0lEXbDIm', 'USER'),
(25, b'1', b'0', b'0', b'1', 'afelkino@discuz.net', '$2a$10$zTbg6nu1J67q6E9DlwbJfeNt2wRmxn3oAdmop2aMZxQD6DcIyHe7y', 'USER'),
(26, b'1', b'0', b'1', b'1', 'vianellip@webmd.com', '$2a$10$alLNiHXeHk9nI8ecFyQcse14Js88/c3R1b3QddTOe3sm2TIMtDRuy', 'USER'),
(27, b'1', b'1', b'1', b'1', 'wdamperq@hud.gov', '$2a$10$2QtCUf69zSAyLWTgNEmVMOhurV2PgSL32dhBFYulJO91d0BG0Onhi', 'USER'),
(28, b'0', b'1', b'1', b'1', 'cpanchenr@mozilla.com', '$2a$10$96jQneF6k.SAY4P5sF9Ys.akyJ4sUp5GBHsFO7vMTN27TM7bZR/zC', 'USER'),
(29, b'0', b'0', b'0', b'1', 'hdeverilles@china.com.cn', '$2a$10$L94xVK7Ce.IaZ70V2uth0OlTWIN5EC2N6c8HISlchN.cSU36GBzTu', 'USER'),
(30, b'1', b'0', b'1', b'1', 'cchengt@shinystat.com', '$2a$10$0yWrYq7zR.nJ1tNbyVpCf.4xTnYKNM2W2lMvUiEgLxjAPrpl.OZJe', 'USER'),
(31, b'0', b'1', b'1', b'0', 'hrozu@cdc.gov', '$2a$10$vtiv0aUwyqnCHFsQeUvfPevk9HShEbWyGqwV3BsOXvawLIlUpiNe6', 'USER'),
(32, b'0', b'1', b'1', b'0', 'lschultzv@cloudflare.com', '$2a$10$XBqONw2G8OI5PnQ.OKy2ou2BfvGFqneJoNT75eq7Js2mdTo2qNZ7K', 'USER'),
(33, b'1', b'1', b'1', b'0', 'rwisedalew@posterous.com', '$2a$10$qgRfN8SOT2UDlLB8UUQWQeeUhW0otS3l/fEXgWp6gFNRx.q5Te4xO', 'USER'),
(34, b'1', b'0', b'1', b'1', 'lhemshallx@npr.org', '$2a$10$cJLTymVtJIrS5OBhnCc/2u8ZWIw2kdEA9SkUVEfLg9ekzubaQB/8i', 'USER'),
(35, b'0', b'1', b'0', b'0', 'kkenwelly@tiny.cc', '$2a$10$bXv8btKZuskpFc24c1NcPO3wHGWQkQO94lnePn.7TPm6ykX8Qz5pu', 'USER'),
(36, b'1', b'1', b'1', b'1', 'jpollardz@google.it', '$2a$10$mfmIegNWWptHw.K0SwgI9OU3Zo.rJmmqT938KN.B1Ou4p9cLFcw0O', 'USER'),
(37, b'1', b'0', b'0', b'1', 'pcattlemull10@shutterfly.com', '$2a$10$yMbgRbv027hXUGnmAQ/65OU4DcUFgUdKmAhzVKAW5.d1sfwKB1DaS', 'USER'),
(38, b'0', b'1', b'0', b'0', 'ksambrok11@facebook.com', '$2a$10$Lg62StIPBuSdP/4UUyDgoeKSGtabaKWeKlBgcQ9mS5I9EeYQ6y8za', 'USER'),
(39, b'1', b'0', b'0', b'0', 'tsmeeth12@ask.com', '$2a$10$z/.yDIkAUk5YAFVFKpa8feufLwRghJb4Hp9fPrH6IERbCsI0S5dv6', 'USER'),
(40, b'1', b'0', b'1', b'1', 'dconquest13@devhub.com', '$2a$10$deIQi2egrrwUc3DuUbe0iOIbu3mOvlnxGxp1U/D4ZsJKvqqhZJtnW', 'USER'),
(41, b'0', b'0', b'1', b'1', 'hpetegree14@latimes.com', '$2a$10$j8/wuXZ4NcalU4UO5bW4deJWKxLoTjkbu8cUCSf8fHUMaZSQZdJHG', 'USER'),
(42, b'0', b'1', b'1', b'0', 'bgilhooley15@amazonaws.com', '$2a$10$cwldtiJ9gcEvaKNsD/XTHeZlN3jqO8XmEa8w01T2oNAgOjrSmf1QW', 'USER'),
(43, b'0', b'0', b'0', b'1', 'cgrimsey16@sfgate.com', '$2a$10$lLNcEIWtv8x8hBn0N1I/QO.ftUEjjSeT1KLVyoZVhPUD0z5cZTOmC', 'USER'),
(44, b'1', b'1', b'1', b'0', 'qbeney17@ucsd.edu', '$2a$10$QVSVsTpCHE5nbGtaFukqRedFZkiOh6YKVexN05jXFGVk9CmXXf91S', 'USER'),
(45, b'0', b'1', b'0', b'1', 'rhamlington18@devhub.com', '$2a$10$AgRZ57Wngblm9S5G.8bX4ObinTtVEq/5f3nTvgOdipyIgav3R.kiG', 'USER'),
(46, b'0', b'0', b'1', b'1', 'delles19@zdnet.com', '$2a$10$AWrU49CexKOJTy.aMDvmMe5LwUtpzMaejjJ8o9GA2vysIWiZQ23m2', 'USER'),
(47, b'1', b'0', b'0', b'0', 'ecantrill1a@oaic.gov.au', '$2a$10$CxEfJoUTlUQnWqA9.tCiheyzjqO59NiW0rwDmRnHnnFjvmNztEvtW', 'USER'),
(48, b'1', b'0', b'0', b'0', 'wmacfadzean1b@google.es', '$2a$10$YXguSftiavjHojwBkZkNNOKHh7.HO6jPH0e94vfyVwborwuLUOCiy', 'USER'),
(49, b'0', b'0', b'0', b'1', 'dnewvell1c@hud.gov', '$2a$10$oBPnU31CGNGFwaaTL.w1sOBi2fZjPYS76TArHXAjTsLxUN9zRsOCO', 'USER'),
(50, b'0', b'0', b'0', b'0', 'mcawston1d@businessinsider.com', '$2a$10$NnPnkKY86fcdASN4nUG0wOb0SuxLCFuD5qJAYWZYIJVaHw4.VP6pe', 'USER'),
(51, b'1', b'0', b'1', b'0', 'mtoland1e@acquirethisname.com', '$2a$10$mtu6tKN.1zEcQkWhOXmFyuijWfdrDXs8X1No6fMYWaPSH8fqQ9TK.', 'USER'),
(52, b'1', b'1', b'0', b'0', 'cdowle1f@yahoo.com', '$2a$10$hy6XOuZFfyNmlYqVCgxQk.tr6EBz9oMo/YlN/vlbXo2nK2qYpaf4W', 'USER'),
(53, b'0', b'0', b'0', b'0', 'mreneke1g@aboutads.info', '$2a$10$EgCi.CzXePt3claD6EJtBuzvpWX/KBruc6vb5W5Qc6WRiY4BcjwHy', 'USER'),
(54, b'1', b'1', b'1', b'0', 'hrosenblatt1h@list-manage.com', '$2a$10$Vh5fmEeiekFZcTeIJoJJMe0DMci2qzLGpO6YHKz.Uv3oqHJyiLps6', 'USER'),
(55, b'0', b'1', b'0', b'1', 'cpitt1i@unesco.org', '$2a$10$wro.NZZvnMjClYIvuqbVYeYXvWF2./1zBrOhabBzMUfajsQC66UJ2', 'USER'),
(56, b'1', b'0', b'1', b'1', 'shedaux1j@rediff.com', '$2a$10$QGtNWceaUB2fOvDSOldIYe45o/FvJ.ptor9BVLzLrTofclDmrYVw2', 'USER'),
(57, b'1', b'1', b'0', b'0', 'hnesby1k@yolasite.com', '$2a$10$DMyq8NbmlVwFqSwgcrjpE.VY7PUd50jyuCv1Z05HhpmFolsiNYmUK', 'USER'),
(58, b'0', b'0', b'1', b'1', 'njecks1l@reddit.com', '$2a$10$tOb/7dkF3NeXn6EXeOJfLuTzey7xA9gYwJ1Xr04KvOAVw.fiP.5zW', 'USER'),
(59, b'0', b'1', b'1', b'1', 'dgulk1m@marketwatch.com', '$2a$10$ydFBXqWauWaolX2xyL1YF.1RqiTdKUWKTe/HNBwyT01ZxJ0Mq/2R.', 'USER'),
(60, b'1', b'0', b'1', b'0', 'csmerdon1n@hud.gov', '$2a$10$1i1ahCIRnNU.rhYcNjCEceD2EAzkNxUpZEO.3FMalTSmBLNwI7JPC', 'USER'),
(61, b'1', b'1', b'0', b'0', 'abrager1o@w3.org', '$2a$10$MGOhXchdKT8uDodaAmtmPuDAGEQI5CvOX1jbfwuDvd1Wir.hRO0A.', 'USER'),
(62, b'0', b'0', b'1', b'0', 'mdevenny1p@unicef.org', '$2a$10$LcbEZnG32zPLUcoEpouHG.oaadgUqiTgsMJ/18pn27etUdUWmq7bG', 'USER'),
(63, b'0', b'1', b'0', b'0', 'cthomel1q@aol.com', '$2a$10$lcwuwoWCe1QcSxk50K0WD.ArFhSNm6wY54Y3PlZNYd.TAdiovQae2', 'USER'),
(64, b'1', b'0', b'0', b'0', 'dedmenson1r@tripod.com', '$2a$10$IixynHLqO/MCN20XVAyhwOQjmvtGxljRfRz9FJMUdUx.hVsb9Qbgm', 'USER'),
(65, b'1', b'1', b'1', b'0', 'mgraham1s@disqus.com', '$2a$10$tmk5WBLaCM0SZcS7ye/Xwe2pBSkJFSo87N98LmEKOz5ejkWrmhbaC', 'USER'),
(66, b'1', b'1', b'1', b'0', 'epetren1t@walmart.com', '$2a$10$mB.t3WqUEa84t85wAi68ue2wKkxkmL9YGEhh1VV3bhTobKOHTOaom', 'USER'),
(67, b'0', b'0', b'1', b'1', 'pgaspar1u@live.com', '$2a$10$pl.Fnf8L0PLhlMwxRtEMtecbQ9n/iVwPqojje32Q8oUJ/Z89EufXy', 'USER'),
(68, b'0', b'1', b'1', b'1', 'lcamm1v@plala.or.jp', '$2a$10$fyDxM7qzVPYk6b7N1se.ROLxsIcYy1n8aSIfOH3yAioBXINwJD.7G', 'USER'),
(69, b'0', b'0', b'1', b'0', 'rsemon1w@adobe.com', '$2a$10$mh.apSLAg/zCi6PqOpstGO8sHhp3BTngp2mleUvRfnRLKpBGE33z2', 'USER'),
(70, b'1', b'0', b'0', b'0', 'battaway1x@thetimes.co.uk', '$2a$10$ae/gREqBz54arzkuEbs8Ve7.sx1yotUxYv1JNdZ76usDm2nV.Z5Jm', 'USER'),
(71, b'1', b'1', b'0', b'0', 'bdevillier1y@tiny.cc', '$2a$10$4AZKwXaIAvLdZu3uLgh5Ge3gncynmrhk6T4TZJDfGj92sfq0u4N7q', 'USER'),
(72, b'1', b'0', b'1', b'1', 'lsemeniuk1z@telegraph.co.uk', '$2a$10$mY6zdIwx2kcFsgG.LEDN9.SwHQXYgk1sQkg3oeFeZODhE4mkpZg6K', 'USER'),
(73, b'1', b'1', b'1', b'1', 'hgeeves20@ucsd.edu', '$2a$10$qHPNebNLzMJyHKhRzCrLAOPWHbznJtjpz/sCkQv.O54yGdRdYTveC', 'USER'),
(74, b'0', b'1', b'1', b'0', 'vgorler21@mozilla.com', '$2a$10$fsTbTOPIuMdI.MndOxCgROipRYusiUimMitWrreiGqDfrWYKVGAgC', 'USER'),
(75, b'1', b'1', b'0', b'1', 'dfarish22@bravesites.com', '$2a$10$T3LgEHMhH.00hvpRH8rDo.MORffHNt/29LUDRhDFaRcNRo5Me8VE2', 'USER'),
(76, b'0', b'0', b'1', b'0', 'bdingle23@google.cn', '$2a$10$j.i7dqmcpQPbiPaRnjEc/On1BflNL/WqoSI9./KFoJJr5EMZG8OTC', 'USER'),
(77, b'1', b'0', b'1', b'0', 'cstranieri24@phpbb.com', '$2a$10$hhDaccLnmG2zyqmUU7cw0O39tr8niYltpDYJchtRpKecQbHdkN9ci', 'USER'),
(78, b'0', b'0', b'1', b'0', 'dpaybody25@wunderground.com', '$2a$10$lRZv9Hp63RVypuw13/9pfe5Mj2k70iZ2T5OEcjNE/FjTZsrGgeDK.', 'USER'),
(79, b'0', b'1', b'0', b'1', 'mgarcia26@rambler.ru', '$2a$10$8iOZRss6F8jZQTqBHHD1A.aGRV07lPXxpkC.2VH7m0nsdd/a7ryWC', 'USER'),
(80, b'1', b'0', b'1', b'0', 'vcecchetelli27@miibeian.gov.cn', '$2a$10$ECWJpRw49Jmwzf/0nR7ZDu1mKrPXicctkIHfoVvRKt8WDs2WmsEB6', 'USER'),
(81, b'0', b'1', b'1', b'1', 'ttaleworth28@yahoo.co.jp', '$2a$10$vm.imCts1OtUQxYoR9WfbOqU849Aa7A0TUqaxEbJRcDE2Yyypy8P6', 'USER'),
(82, b'0', b'1', b'1', b'1', 'cgillespie29@mozilla.org', '$2a$10$q6T39hNmDfZl7RLNQoAEZeSCOVs37cyb6RRZq9DKspvKw60lZEsIe', 'USER'),
(83, b'1', b'0', b'0', b'0', 'lklink2a@1688.com', '$2a$10$BGZJYfmV6sbKNaBilThrWOtGC.2.HiE99P6.uoojwhCxIUQR/YYXi', 'USER'),
(84, b'0', b'0', b'0', b'0', 'jbenford2b@sfgate.com', '$2a$10$YKXaDyYnEnhZ5.yS7t/iiOmwYMKunhxdI0U8YJILaAXTt/Msa5J3e', 'USER'),
(85, b'1', b'0', b'1', b'0', 'ncastledine2c@arstechnica.com', '$2a$10$o89HwRdL08Xpn0Y9Pfe6yeCs9N3cKtdkJTQt.fr2HxlGrmuGVJu76', 'USER'),
(86, b'0', b'1', b'1', b'0', 'jlandal2d@arizona.edu', '$2a$10$5MyteAUPWaB0tpmCtOX8r./ptYy90KX1owsbKeIRQ9XpNsHNzzwly', 'USER'),
(87, b'1', b'0', b'0', b'1', 'gluxton2e@reverbnation.com', '$2a$10$K7LNsorqImmzQXP0RZJwj.wE6w5BWPly6NMFebPKK83vZVBuBt3Mu', 'USER'),
(88, b'0', b'0', b'0', b'1', 'gshapland2f@mail.ru', '$2a$10$qNRX9EXvlWYJldnd0kZHe.c.9pl3W9SdFhVhSdVE7YyedMQ5diThm', 'USER'),
(89, b'0', b'1', b'0', b'1', 'cgreswell2g@taobao.com', '$2a$10$yw2J2odVrXHv/ltYz1JR1.Kg7wKAorod.KgtSjyV4D4ww7ERmR17C', 'USER'),
(90, b'0', b'0', b'0', b'1', 'kluce2h@spiegel.de', '$2a$10$2YlX7LUELsFgNQCAIIiEGexiBPcidtnhr15YPjzEMCZ5yWtlg1SIu', 'USER'),
(91, b'0', b'0', b'1', b'1', 'dhasnip2i@wordpress.com', '$2a$10$AJhiSmceeCdy/gYucVOWpOF2cYzx13h6W2wwYCulwRjYJ2NtZO/ni', 'USER'),
(92, b'1', b'0', b'0', b'0', 'fbrackenridge2j@latimes.com', '$2a$10$Cr2CmL.Q/.A.b9uHAA5FKuQPPR.F8u0Og/9MKCKc/mNDad.T4NvbW', 'USER'),
(93, b'1', b'1', b'1', b'1', 'rmattessen2k@t-online.de', '$2a$10$Jtrnioboxn/RHctZ8IYgv.Xog5Co1R9H3EmzUWSKKmHhF6NBH.Wfa', 'USER'),
(94, b'1', b'0', b'1', b'1', 'kguyonneau2l@google.com.hk', '$2a$10$r43jMqzJdOBKjTTxN6YJQeR/ymOu7IgX3ny0CYqdK7nSMGv530XNW', 'USER'),
(95, b'0', b'0', b'1', b'1', 'fjimenez2m@timesonline.co.uk', '$2a$10$UzuqWM89xmDBCbPzahJw/uNC7gVVpM2VTgboKjElWoJlSlYLrBqJG', 'USER'),
(96, b'1', b'0', b'1', b'1', 'rivanyukov2n@census.gov', '$2a$10$/q5yFUqoP0zY998WSEMQt.FyU0L.h68Tcqvhz8DfyEGiCJ8hYwP26', 'USER'),
(97, b'1', b'0', b'0', b'1', 'cwarin2o@businessinsider.com', '$2a$10$W.idCgrctp3UA3n5Ga5MwOB4guYaLdYDV3.1LXPVosOX3n9jnVv7G', 'USER'),
(98, b'1', b'1', b'0', b'0', 'relement2p@sohu.com', '$2a$10$nTij7fvKfcwEbH3oictxVucuvfpetDZHiUgX.r9AX4S8.cVdOayRa', 'USER'),
(99, b'0', b'0', b'1', b'0', 'pelsmore2q@jigsy.com', '$2a$10$iWr9c3aRYDdrmgpxz2qwWO1C.DKTY4P.cQ4i4Jmr0zQLT3YslAON.', 'USER'),
(100, b'1', b'0', b'1', b'1', 'mfarlow2r@phoca.cz', '$2a$10$Mjj1hHqbqwsjzv8qy2ukOODwIU6AQLRDmW4qPmerRa7kzDfswau3S', 'USER');

insert into user (user_id, first_name, last_name, account_id) values (1, 'Yè', 'Matcham', 1);
insert into user (user_id, first_name, last_name, account_id) values (2, 'Wá', 'Gaspard', 2);
insert into user (user_id, first_name, last_name, account_id) values (3, 'Desirée', 'Frain', 3);
insert into user (user_id, first_name, last_name, account_id) values (4, 'Dafnée', 'Ryott', 4);
insert into user (user_id, first_name, last_name, account_id) values (5, 'Béatrice', 'Drewet', 5);
insert into user (user_id, first_name, last_name, account_id) values (6, 'Daphnée', 'Dufaire', 6);
insert into user (user_id, first_name, last_name, account_id) values (7, 'Maëline', 'Humphris', 7);
insert into user (user_id, first_name, last_name, account_id) values (8, 'Lài', 'Lefwich', 8);
insert into user (user_id, first_name, last_name, account_id) values (9, 'Maëlyss', 'Will', 9);
insert into user (user_id, first_name, last_name, account_id) values (10, 'Yénora', 'Scollick', 10);
insert into user (user_id, first_name, last_name, account_id) values (11, 'Stéphanie', 'Chadburn', 11);
insert into user (user_id, first_name, last_name, account_id) values (12, 'Béatrice', 'Farden', 12);
insert into user (user_id, first_name, last_name, account_id) values (13, 'Frédérique', 'Parlett', 13);
insert into user (user_id, first_name, last_name, account_id) values (14, 'Inès', 'Chieco', 14);
insert into user (user_id, first_name, last_name, account_id) values (15, 'Aí', 'Grigorini', 15);
insert into user (user_id, first_name, last_name, account_id) values (16, 'Régine', 'Maundrell', 16);
insert into user (user_id, first_name, last_name, account_id) values (17, 'Kuí', 'Remon', 17);
insert into user (user_id, first_name, last_name, account_id) values (18, 'Alizée', 'De Gregario', 18);
insert into user (user_id, first_name, last_name, account_id) values (19, 'Lauréna', 'Pruckner', 19);
insert into user (user_id, first_name, last_name, account_id) values (20, 'Kallisté', 'Saulter', 20);
insert into user (user_id, first_name, last_name, account_id) values (21, 'Mårten', 'Brougham', 21);
insert into user (user_id, first_name, last_name, account_id) values (22, 'Léonie', 'MacAirt', 22);
insert into user (user_id, first_name, last_name, account_id) values (23, 'Crééz', 'Saket', 23);
insert into user (user_id, first_name, last_name, account_id) values (24, 'Annotés', 'Breton', 24);
insert into user (user_id, first_name, last_name, account_id) values (25, 'Bécassine', 'Rozsa', 25);
insert into user (user_id, first_name, last_name, account_id) values (26, 'Daphnée', 'Lafuente', 26);
insert into user (user_id, first_name, last_name, account_id) values (27, 'Åke', 'Smitton', 27);
insert into user (user_id, first_name, last_name, account_id) values (28, 'Alizée', 'Mechan', 28);
insert into user (user_id, first_name, last_name, account_id) values (29, 'Pò', 'Skittle', 29);
insert into user (user_id, first_name, last_name, account_id) values (30, 'Andrée', 'Whellams', 30);
insert into user (user_id, first_name, last_name, account_id) values (31, 'Léa', 'Bossons', 31);
insert into user (user_id, first_name, last_name, account_id) values (32, 'Eléonore', 'Ather', 32);
insert into user (user_id, first_name, last_name, account_id) values (33, 'Angélique', 'Duke', 33);
insert into user (user_id, first_name, last_name, account_id) values (34, 'Noémie', 'Cirlos', 34);
insert into user (user_id, first_name, last_name, account_id) values (35, 'Yú', 'Fowlds', 35);
insert into user (user_id, first_name, last_name, account_id) values (36, 'Miléna', 'Leithgoe', 36);
insert into user (user_id, first_name, last_name, account_id) values (37, 'Miléna', 'Godden', 37);
insert into user (user_id, first_name, last_name, account_id) values (38, 'Mahélie', 'Mateiko', 38);
insert into user (user_id, first_name, last_name, account_id) values (39, 'Liè', 'Janouch', 39);
insert into user (user_id, first_name, last_name, account_id) values (40, 'Esbjörn', 'Casse', 40);
insert into user (user_id, first_name, last_name, account_id) values (41, 'Maëlle', 'Lambrecht', 41);
insert into user (user_id, first_name, last_name, account_id) values (42, 'Andréa', 'Prydden', 42);
insert into user (user_id, first_name, last_name, account_id) values (43, 'Laurélie', 'Dovermann', 43);
insert into user (user_id, first_name, last_name, account_id) values (44, 'Audréanne', 'Acom', 44);
insert into user (user_id, first_name, last_name, account_id) values (45, 'Märta', 'Rushton', 45);
insert into user (user_id, first_name, last_name, account_id) values (46, 'Fèi', 'Di Biaggi', 46);
insert into user (user_id, first_name, last_name, account_id) values (47, 'Anaël', 'Gofton', 47);
insert into user (user_id, first_name, last_name, account_id) values (48, 'Kévina', 'Kilminster', 48);
insert into user (user_id, first_name, last_name, account_id) values (49, 'Mårten', 'Whiffen', 49);
insert into user (user_id, first_name, last_name, account_id) values (50, 'Mylène', 'Oakshott', 50);
insert into user (user_id, first_name, last_name, account_id) values (51, 'Zoé', 'Annakin', 51);
insert into user (user_id, first_name, last_name, account_id) values (52, 'Lèi', 'Gallaway', 52);
insert into user (user_id, first_name, last_name, account_id) values (53, 'Zoé', 'Lettington', 53);
insert into user (user_id, first_name, last_name, account_id) values (54, 'Nuó', 'Blum', 54);
insert into user (user_id, first_name, last_name, account_id) values (55, 'Ruì', 'Leaf', 55);
insert into user (user_id, first_name, last_name, account_id) values (56, 'Östen', 'McGinley', 56);
insert into user (user_id, first_name, last_name, account_id) values (57, 'Mahélie', 'McVee', 57);
insert into user (user_id, first_name, last_name, account_id) values (58, 'Åslög', 'Gentil', 58);
insert into user (user_id, first_name, last_name, account_id) values (59, 'Inès', 'Dany', 59);
insert into user (user_id, first_name, last_name, account_id) values (60, 'Aurélie', 'Pawlick', 60);
insert into user (user_id, first_name, last_name, account_id) values (61, 'Clémentine', 'McKellen', 61);
insert into user (user_id, first_name, last_name, account_id) values (62, 'Mélys', 'Clutten', 62);
insert into user (user_id, first_name, last_name, account_id) values (63, 'Marlène', 'Bowart', 63);
insert into user (user_id, first_name, last_name, account_id) values (64, 'Léa', 'Hakonsen', 64);
insert into user (user_id, first_name, last_name, account_id) values (65, 'Yénora', 'Perigeaux', 65);
insert into user (user_id, first_name, last_name, account_id) values (66, 'Dà', 'Plumbley', 66);
insert into user (user_id, first_name, last_name, account_id) values (67, 'Cécilia', 'Damarell', 67);
insert into user (user_id, first_name, last_name, account_id) values (68, 'Maëlyss', 'Petyt', 68);
insert into user (user_id, first_name, last_name, account_id) values (69, 'Irène', 'McNickle', 69);
insert into user (user_id, first_name, last_name, account_id) values (70, 'Maïté', 'Dymond', 70);
insert into user (user_id, first_name, last_name, account_id) values (71, 'Göran', 'Krojn', 71);
insert into user (user_id, first_name, last_name, account_id) values (72, 'Laurélie', 'Snedker', 72);
insert into user (user_id, first_name, last_name, account_id) values (73, 'Yóu', 'Lerway', 73);
insert into user (user_id, first_name, last_name, account_id) values (74, 'André', 'O''Keenan', 74);
insert into user (user_id, first_name, last_name, account_id) values (75, 'Andrée', 'Jobbing', 75);
insert into user (user_id, first_name, last_name, account_id) values (76, 'Maïly', 'Gerrie', 76);
insert into user (user_id, first_name, last_name, account_id) values (77, 'Jú', 'Martusov', 77);
insert into user (user_id, first_name, last_name, account_id) values (78, 'Adèle', 'Seathwright', 78);
insert into user (user_id, first_name, last_name, account_id) values (79, 'Sélène', 'Abernethy', 79);
insert into user (user_id, first_name, last_name, account_id) values (80, 'Océane', 'Meadley', 80);
insert into user (user_id, first_name, last_name, account_id) values (81, 'Laurélie', 'Sibthorp', 81);
insert into user (user_id, first_name, last_name, account_id) values (82, 'Pélagie', 'de Nore', 82);
insert into user (user_id, first_name, last_name, account_id) values (83, 'Pélagie', 'Thorington', 83);
insert into user (user_id, first_name, last_name, account_id) values (84, 'Lorène', 'Lamden', 84);
insert into user (user_id, first_name, last_name, account_id) values (85, 'Rébecca', 'Escalante', 85);
insert into user (user_id, first_name, last_name, account_id) values (86, 'Noémie', 'Redan', 86);
insert into user (user_id, first_name, last_name, account_id) values (87, 'Cécile', 'Madison', 87);
insert into user (user_id, first_name, last_name, account_id) values (88, 'Michèle', 'Fricker', 88);
insert into user (user_id, first_name, last_name, account_id) values (89, 'Gaëlle', 'Fawks', 89);
insert into user (user_id, first_name, last_name, account_id) values (90, 'Michèle', 'Di Napoli', 90);
insert into user (user_id, first_name, last_name, account_id) values (91, 'Angélique', 'Ciccotti', 91);
insert into user (user_id, first_name, last_name, account_id) values (92, 'Tán', 'Leupoldt', 92);
insert into user (user_id, first_name, last_name, account_id) values (93, 'Maéna', 'Haldon', 93);
insert into user (user_id, first_name, last_name, account_id) values (94, 'Alizée', 'Lambertazzi', 94);
insert into user (user_id, first_name, last_name, account_id) values (95, 'Maëlle', 'Sewter', 95);
insert into user (user_id, first_name, last_name, account_id) values (96, 'Cléopatre', 'Eagan', 96);
insert into user (user_id, first_name, last_name, account_id) values (97, 'Marie-josée', 'Beauvais', 97);
insert into user (user_id, first_name, last_name, account_id) values (98, 'Maëlann', 'Leipelt', 98);
insert into user (user_id, first_name, last_name, account_id) values (99, 'Laïla', 'Corrison', 99);
insert into user (user_id, first_name, last_name, account_id) values (100, 'Pénélope', 'Gillow', 100);

insert into wallet (wallet_id, balance, currency, user_id) values (1, 98.03, 'CZK', 1);
insert into wallet (wallet_id, balance, currency, user_id) values (2, 29.71, 'PHP', 2);
insert into wallet (wallet_id, balance, currency, user_id) values (3, 96.45, 'NGN', 3);
insert into wallet (wallet_id, balance, currency, user_id) values (4, 21.17, 'CNY', 4);
insert into wallet (wallet_id, balance, currency, user_id) values (5, 82.3, 'BRL', 5);
insert into wallet (wallet_id, balance, currency, user_id) values (6, 68.93, 'JPY', 6);
insert into wallet (wallet_id, balance, currency, user_id) values (7, 86.9, 'IDR', 7);
insert into wallet (wallet_id, balance, currency, user_id) values (8, 35.65, 'PHP', 8);
insert into wallet (wallet_id, balance, currency, user_id) values (9, 59.47, 'PKR', 9);
insert into wallet (wallet_id, balance, currency, user_id) values (10, 49.67, 'PHP', 10);
insert into wallet (wallet_id, balance, currency, user_id) values (11, 14.02, 'USD', 11);
insert into wallet (wallet_id, balance, currency, user_id) values (12, 73.29, 'CNY', 12);
insert into wallet (wallet_id, balance, currency, user_id) values (13, 59.22, 'IDR', 13);
insert into wallet (wallet_id, balance, currency, user_id) values (14, 45.05, 'IDR', 14);
insert into wallet (wallet_id, balance, currency, user_id) values (15, 67.82, 'CNY', 15);
insert into wallet (wallet_id, balance, currency, user_id) values (16, 21.22, 'CNY', 16);
insert into wallet (wallet_id, balance, currency, user_id) values (17, 74.92, 'CNY', 17);
insert into wallet (wallet_id, balance, currency, user_id) values (18, 38.49, 'RUB', 18);
insert into wallet (wallet_id, balance, currency, user_id) values (19, 11.93, 'BRL', 19);
insert into wallet (wallet_id, balance, currency, user_id) values (20, 74.59, 'TZS', 20);
insert into wallet (wallet_id, balance, currency, user_id) values (21, 37.73, 'COP', 21);
insert into wallet (wallet_id, balance, currency, user_id) values (22, 70.07, 'MNT', 22);
insert into wallet (wallet_id, balance, currency, user_id) values (23, 36.24, 'PLN', 23);
insert into wallet (wallet_id, balance, currency, user_id) values (24, 38.06, 'RUB', 24);
insert into wallet (wallet_id, balance, currency, user_id) values (25, 46.16, 'UAH', 25);
insert into wallet (wallet_id, balance, currency, user_id) values (26, 70.83, 'BRL', 26);
insert into wallet (wallet_id, balance, currency, user_id) values (27, 92.33, 'PYG', 27);
insert into wallet (wallet_id, balance, currency, user_id) values (28, 29.15, 'EUR', 28);
insert into wallet (wallet_id, balance, currency, user_id) values (29, 67.02, 'SYP', 29);
insert into wallet (wallet_id, balance, currency, user_id) values (30, 42.04, 'HTG', 30);
insert into wallet (wallet_id, balance, currency, user_id) values (31, 45.92, 'IDR', 31);
insert into wallet (wallet_id, balance, currency, user_id) values (32, 10.67, 'HRK', 32);
insert into wallet (wallet_id, balance, currency, user_id) values (33, 90.05, 'EUR', 33);
insert into wallet (wallet_id, balance, currency, user_id) values (34, 18.93, 'CNY', 34);
insert into wallet (wallet_id, balance, currency, user_id) values (35, 56.39, 'EUR', 35);
insert into wallet (wallet_id, balance, currency, user_id) values (36, 59.45, 'GTQ', 36);
insert into wallet (wallet_id, balance, currency, user_id) values (37, 25.86, 'RUB', 37);
insert into wallet (wallet_id, balance, currency, user_id) values (38, 52.1, 'NZD', 38);
insert into wallet (wallet_id, balance, currency, user_id) values (39, 75.46, 'PLN', 39);
insert into wallet (wallet_id, balance, currency, user_id) values (40, 23.07, 'IDR', 40);
insert into wallet (wallet_id, balance, currency, user_id) values (41, 23.44, 'CNY', 41);
insert into wallet (wallet_id, balance, currency, user_id) values (42, 70.1, 'HUF', 42);
insert into wallet (wallet_id, balance, currency, user_id) values (43, 22.09, 'COP', 43);
insert into wallet (wallet_id, balance, currency, user_id) values (44, 82.15, 'IDR', 44);
insert into wallet (wallet_id, balance, currency, user_id) values (45, 86.08, 'COP', 45);
insert into wallet (wallet_id, balance, currency, user_id) values (46, 37.55, 'EUR', 46);
insert into wallet (wallet_id, balance, currency, user_id) values (47, 53.97, 'USD', 47);
insert into wallet (wallet_id, balance, currency, user_id) values (48, 27.22, 'CNY', 48);
insert into wallet (wallet_id, balance, currency, user_id) values (49, 89.85, 'BRL', 49);
insert into wallet (wallet_id, balance, currency, user_id) values (50, 38.27, 'EUR', 50);
insert into wallet (wallet_id, balance, currency, user_id) values (51, 26.9, 'XOF', 51);
insert into wallet (wallet_id, balance, currency, user_id) values (52, 94.41, 'CDF', 52);
insert into wallet (wallet_id, balance, currency, user_id) values (53, 12.84, 'HTG', 53);
insert into wallet (wallet_id, balance, currency, user_id) values (54, 69.95, 'CNY', 54);
insert into wallet (wallet_id, balance, currency, user_id) values (55, 65.92, 'MNT', 55);
insert into wallet (wallet_id, balance, currency, user_id) values (56, 61.53, 'BRL', 56);
insert into wallet (wallet_id, balance, currency, user_id) values (57, 18.21, 'CAD', 57);
insert into wallet (wallet_id, balance, currency, user_id) values (58, 88.04, 'IDR', 58);
insert into wallet (wallet_id, balance, currency, user_id) values (59, 21.33, 'BRL', 59);
insert into wallet (wallet_id, balance, currency, user_id) values (60, 75.2, 'COP', 60);
insert into wallet (wallet_id, balance, currency, user_id) values (61, 27.85, 'PHP', 61);
insert into wallet (wallet_id, balance, currency, user_id) values (62, 55.18, 'EUR', 62);
insert into wallet (wallet_id, balance, currency, user_id) values (63, 38.88, 'EUR', 63);
insert into wallet (wallet_id, balance, currency, user_id) values (64, 50.87, 'CHF', 64);
insert into wallet (wallet_id, balance, currency, user_id) values (65, 56.29, 'CNY', 65);
insert into wallet (wallet_id, balance, currency, user_id) values (66, 20.89, 'UGX', 66);
insert into wallet (wallet_id, balance, currency, user_id) values (67, 68.33, 'RUB', 67);
insert into wallet (wallet_id, balance, currency, user_id) values (68, 93.16, 'CZK', 68);
insert into wallet (wallet_id, balance, currency, user_id) values (69, 85.87, 'IDR', 69);
insert into wallet (wallet_id, balance, currency, user_id) values (70, 25.58, 'BAM', 70);
insert into wallet (wallet_id, balance, currency, user_id) values (71, 83.91, 'EUR', 71);
insert into wallet (wallet_id, balance, currency, user_id) values (72, 30.87, 'CNY', 72);
insert into wallet (wallet_id, balance, currency, user_id) values (73, 34.97, 'EUR', 73);
insert into wallet (wallet_id, balance, currency, user_id) values (74, 43.98, 'CAD', 74);
insert into wallet (wallet_id, balance, currency, user_id) values (75, 42.63, 'RSD', 75);
insert into wallet (wallet_id, balance, currency, user_id) values (76, 50.3, 'EUR', 76);
insert into wallet (wallet_id, balance, currency, user_id) values (77, 100.0, 'EUR', 77);
insert into wallet (wallet_id, balance, currency, user_id) values (78, 71.24, 'KMF', 78);
insert into wallet (wallet_id, balance, currency, user_id) values (79, 21.21, 'PHP', 79);
insert into wallet (wallet_id, balance, currency, user_id) values (80, 34.7, 'PHP', 80);
insert into wallet (wallet_id, balance, currency, user_id) values (81, 42.52, 'EUR', 81);
insert into wallet (wallet_id, balance, currency, user_id) values (82, 13.08, 'CZK', 82);
insert into wallet (wallet_id, balance, currency, user_id) values (83, 22.89, 'PKR', 83);
insert into wallet (wallet_id, balance, currency, user_id) values (84, 99.47, 'EUR', 84);
insert into wallet (wallet_id, balance, currency, user_id) values (85, 14.48, 'USD', 85);
insert into wallet (wallet_id, balance, currency, user_id) values (86, 30.84, 'EUR', 86);
insert into wallet (wallet_id, balance, currency, user_id) values (87, 34.31, 'CNY', 87);
insert into wallet (wallet_id, balance, currency, user_id) values (88, 13.82, 'CNY', 88);
insert into wallet (wallet_id, balance, currency, user_id) values (89, 66.28, 'BRL', 89);
insert into wallet (wallet_id, balance, currency, user_id) values (90, 84.38, 'SAR', 90);
insert into wallet (wallet_id, balance, currency, user_id) values (91, 47.47, 'ILS', 91);
insert into wallet (wallet_id, balance, currency, user_id) values (92, 58.23, 'CNY', 92);
insert into wallet (wallet_id, balance, currency, user_id) values (93, 90.19, 'RUB', 93);
insert into wallet (wallet_id, balance, currency, user_id) values (94, 79.93, 'CNY', 94);
insert into wallet (wallet_id, balance, currency, user_id) values (95, 32.69, 'BRL', 95);
insert into wallet (wallet_id, balance, currency, user_id) values (96, 77.03, 'UAH', 96);
insert into wallet (wallet_id, balance, currency, user_id) values (97, 51.84, 'BRL', 97);
insert into wallet (wallet_id, balance, currency, user_id) values (98, 49.5, 'EUR', 98);
insert into wallet (wallet_id, balance, currency, user_id) values (99, 47.42, 'CNY', 99);
insert into wallet (wallet_id, balance, currency, user_id) values (100, 96.17, 'MXN', 100);

insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (1, 'CY57 2498 0517 QPOA L1IZ YRNC UIFJ', 'CNKRUBXTDP', 'Whitney MacTerrelly', 1);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (2, 'LB03 5864 IFPT IOVA JDKE CR9O 4ASK', 'VMYBNEPUKW', 'Nicola Esche', 2);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (3, 'HR09 5637 8785 7537 9894 2', 'QWOAEGNDR', 'Lemar Tesseyman', 3);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (4, 'GI42 SNEG JYAV SDDF YHZA LG9', 'CMPLHUTQ', 'Dehlia Spoerl', 4);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (5, 'MD60 3LSG ZHNK 8UEJ NU0N A7L7', 'DXIPALCOFKN', 'Onofredo Lasselle', 5);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (6, 'FR36 9168 0140 59KT JIIU 3LR1 748', 'IFVCNYWSR', 'Eleni Stebbins', 6);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (7, 'BE31 7109 4521 8673', 'SBWVUMAIG', 'Ike Sadler', 7);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (8, 'GE48 PM32 8860 7162 8661 19', 'FTWMLADVPY', 'Hendrika Kann', 8);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (9, 'FR94 7116 8734 34OS 5VPC 9FSM A44', 'VITUGEWNB', 'Ransell Cheeney', 9);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (10, 'CY37 0218 8786 1MG7 OND1 09N4 MYSV', 'LHUXPCJRT', 'Fred Erat', 10);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (11, 'CH78 7018 6O9U CUG5 R2DJ S', 'DEPKOMIGAQR', 'Florence Cousens', 11);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (12, 'IT04 D401 2119 307E ZJVN 5F9H IYK', 'TDULWISV', 'Alena Laker', 12);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (13, 'BH90 BCTA UX63 1AFS IGIH 6Y', 'LGIOJFKXQM', 'Hatty Peabody', 13);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (14, 'KW75 KMKS WCBG 6BQJ FG4R ALRB K27S OR', 'YHSALWPNQ', 'Zola Mazzeo', 14);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (15, 'FR17 2192 5833 383Z L54C JLR9 F05', 'LFZPROEJ', 'Gerhard Gaukroger', 15);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (16, 'RS76 0795 8654 9257 8965 44', 'EZJGMLSYH', 'Abba Spiniello', 16);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (17, 'FR75 1425 2313 07KE CG7F RCFB A91', 'ZNKLOBFYXI', 'Aeriela Heeran', 17);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (18, 'FR87 0282 4454 01D9 DPLX 7BL1 V20', 'QAVWKNOPTXE', 'Nari Hursey', 18);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (19, 'ME64 1659 8864 4952 9738 05', 'JBCUHEGQTKL', 'Stanislaus Kingscote', 19);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (20, 'BR44 1153 9588 4273 8042 3320 128D M', 'QOBYMWRGDHV', 'Mikaela Habbema', 20);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (21, 'GB97 QRGK 3615 0930 7365 06', 'CYEZNQPI', 'Halette Stanhope', 21);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (22, 'BH34 WHIF YQAV BTBT SSXK IN', 'ZXOKAQIEPFU', 'Enrica Waker', 22);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (23, 'BE69 3881 4142 6603', 'OAHTUFYS', 'Shara Doolan', 23);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (24, 'MU94 MZFE 5012 7211 7992 9643 983M TK', 'ICBXJEUPFSG', 'Lezlie Beresfore', 24);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (25, 'TR13 6097 9HNG FWQX IQZH WSUR HO', 'PACRQMFTGLZ', 'Gianna Carefull', 25);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (26, 'PL65 1526 6268 0948 1346 6189 3203', 'ZULQRSOFTBV', 'Astra Brophy', 26);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (27, 'FO97 0163 9277 0782 96', 'MOBLQNRUIX', 'Kelly Catonnet', 27);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (28, 'GI27 WURK FXZU QBFI ZCWG PGV', 'TDGFAIZQ', 'Udall Durbann', 28);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (29, 'PL36 8481 2442 3444 3711 0801 7870', 'KFDIWZJQ', 'Eugene Seath', 29);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (30, 'NL25 ILHX 2656 8535 74', 'ZDSRVCIYBMO', 'Jilleen Sallnow', 30);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (31, 'LV38 YXBM EOW9 VUKN N8Z4 Q', 'LKPEFTZJIC', 'Cleon Feehan', 31);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (32, 'NL78 GCDA 6290 1141 93', 'ZKYLCSHV', 'Portia Bearns', 32);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (33, 'FR43 3748 0712 84DR RTVX XJUH C52', 'HKWPUVTINSY', 'Erinna Wolver', 33);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (34, 'MC70 5759 6625 91KQ ZVAW AME9 B57', 'BYCGUKSL', 'Harriet Insull', 34);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (35, 'SE17 1972 4925 7426 1510 8843', 'SMXEZJOUT', 'Deny Guillerman', 35);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (36, 'MK76 580Q HGHP TMWM L52', 'BOXRANMLY', 'Paxon Goudy', 36);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (37, 'MD96 T114 YVQO LA2H QXCH QWAS', 'XKVSOZGC', 'Rurik Midner', 37);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (38, 'FR08 9387 6131 70JF IW35 NARL W67', 'UZJFYHWAGB', 'Christina Jilliss', 38);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (39, 'GI78 MINW WU13 DNTK 7603 DRV', 'PFTBQWCM', 'Quillan Venners', 39);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (40, 'FO68 6124 2337 5384 55', 'YCIUATDO', 'Amie Scain', 40);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (41, 'CH55 3721 27ZG NBX5 XM2I C', 'VDIRKWCTFA', 'Vera Feedham', 41);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (42, 'PS61 KMOV X4QQ UFPH R5PX XQZQ P7LC Y', 'RLKAJVPC', 'Willette Dyster', 42);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (43, 'ME45 1663 7990 6377 9764 43', 'RLUDNQOK', 'Jard Scothorne', 43);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (44, 'AT74 2781 8556 5011 5089', 'JVLWPMQN', 'Jonell Lagne', 44);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (45, 'LT82 1046 7578 3541 9895', 'QNXOALHJVIY', 'Ricki Gilroy', 45);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (46, 'MK54 586V LHQB VYEV G58', 'QPOSKTHXGND', 'Gabey Croxford', 46);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (47, 'DO31 NGUE 5430 6370 0768 1400 4358', 'CXWNZMHJ', 'Henka Reason', 47);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (48, 'FR20 1117 0938 116Q NB2N M4EF Y31', 'CHETRFWB', 'Haven Krochmann', 48);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (49, 'FR42 5908 4532 04D2 J2VR 3XPU N64', 'KSMGZRJTU', 'Deonne Kniveton', 49);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (50, 'CH76 2250 3I0O UZBQ BML3 P', 'OTJKXYZEQ', 'Charissa Conybear', 50);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (51, 'GT86 QVRA QCGG TANJ EE3S DUSR NN9A', 'UFXVPHECLT', 'Angel Ferrara', 51);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (52, 'BG18 DUUH 9415 56S2 QPYQ TW', 'LZHYWJPRSMA', 'Bethany Knowling', 52);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (53, 'FR50 8792 8928 11RN SSIB GZTR N73', 'YTZWGOSVEIJ', 'Rosina Matiewe', 53);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (54, 'ES91 4127 1755 7297 4825 9774', 'UMBWSNIGOE', 'Traver Buard', 54);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (55, 'MK54 393I FJRN EDOL U44', 'UKVBOHTEJF', 'Rosalie Cicculini', 55);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (56, 'SA07 10IV 68FL T0C9 CCX1 4KJE', 'SRHXEYDUAQ', 'Mannie Jaume', 56);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (57, 'FR06 1600 1551 66NO YP0X Y6ZZ U45', 'TXLOJSKEFI', 'Vanessa De Dantesie', 57);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (58, 'GR51 0064 388C OHM5 ELAF SCDC GM5', 'WMALROGUZED', 'Morna Vescovini', 58);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (59, 'LV55 KTNR J6P5 OHJZ N6WJ D', 'UVTFZYRGHW', 'Lotte Kyttor', 59);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (60, 'KW91 SSBW CVL7 GFCI S2RG N8K5 THZR TU', 'AKCHYVFONW', 'Bessie Pettigrew', 60);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (61, 'ES95 8260 5329 3088 2266 9332', 'DAXLZPQFG', 'Jackqueline Drinkel', 61);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (62, 'FR33 0917 5566 994T U0TB 1VYZ K37', 'AQIYZRXOJ', 'Merrie Cleere', 62);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (63, 'IT07 Z596 2363 171U CRKV H9WO 0NK', 'KLRUENYVB', 'Leelah Chitson', 63);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (64, 'TR69 4714 5AEN ZNT3 GGMJ K4GI I3', 'SLEBAMXOQRZ', 'Mathilde Fewlass', 64);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (65, 'ME27 7602 3600 2847 9906 56', 'GFNMBLOWQ', 'Jo-anne Longson', 65);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (66, 'AE65 5502 3885 1273 3719 125', 'JLQSWPCATHG', 'Shalom Denholm', 66);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (67, 'KW83 DMXS CBFQ SYJ9 ISS2 GBTU HWA4 EA', 'YQZTXDPGKV', 'Saleem Norton', 67);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (68, 'DK11 3936 5123 5072 15', 'HERCZJVTGYI', 'Mil Pittford', 68);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (69, 'LI21 0366 1OAC IHMW QITS M', 'XZUQFKYRDLG', 'Kamilah Mowsley', 69);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (70, 'FR96 7830 6090 85OD B4W4 DES5 B40', 'PAQNHUDE', 'Franciskus Lackner', 70);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (71, 'PT93 5339 1549 8975 6988 6618 7', 'PLNJVYEMSD', 'Shauna Cassels', 71);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (72, 'LU32 165F HJTW HRDN TCD1', 'AHKVEFDUZWJ', 'Frederic Hawkey', 72);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (73, 'AL25 3310 3629 MDZN GBEE FQ0C 6RQO', 'YQVORPUA', 'Stacy Harborow', 73);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (74, 'DO35 E7YV 6692 4503 1692 7530 5413', 'RCPFMQYZGW', 'Meris Varfolomeev', 74);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (75, 'BG60 AUYI 1105 480B QACQ CW', 'YNUMCFDVJXL', 'Wilden Bransby', 75);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (76, 'LI58 0572 4GKF U7FE CIOU M', 'ONUZDJYM', 'Codie Ciciari', 76);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (77, 'GR39 8576 293A CYIU 2NFB E9MN ITX', 'QPBHMAXUO', 'Germana Premble', 77);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (78, 'FR05 1615 7965 97EW XTBJ VN1L 820', 'CTZYFRGJL', 'Aida Baszkiewicz', 78);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (79, 'MD84 YGAW MOBJ YV8Q RNFQ 0DL0', 'YIDNKOFVR', 'Tobe Deeley', 79);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (80, 'FI64 3256 4719 6099 28', 'AQXGJSPTZVK', 'Blanche Spillett', 80);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (81, 'FR75 6846 3438 464R SE3V 0OON Y88', 'VFWAEHIMX', 'Revkah Albertson', 81);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (82, 'FR02 7967 4018 67Y2 S5YU KD78 Q82', 'QOETJHZL', 'Emmett Cottom', 82);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (83, 'BH96 SFLV UICZ SBFD TTR5 78', 'CSRJZMXHUO', 'Arin Raubenheimers', 83);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (84, 'LI05 4089 2YR0 0HPR HLCB V', 'SAXQIRLGCFN', 'Roseanne Goreway', 84);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (85, 'AL56 8726 6182 WF73 PQKX MOVN 20WC', 'SIKRVGDZQT', 'Emile Hulkes', 85);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (86, 'RS94 8627 9970 5500 2760 64', 'NPERUKYW', 'Baron Gresswood', 86);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (87, 'LU05 496Y DRDZ HUQS VYFJ', 'YXVQGLSRUFK', 'Marjory Womersley', 87);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (88, 'SE10 4744 7541 8003 3383 7140', 'GTLJMAZFB', 'Phineas Hinckesman', 88);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (89, 'GR39 0052 3918 JPZ2 18TX 4JCB PQI', 'ABMRNKLJIP', 'Shannan Vamplus', 89);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (90, 'FR35 1904 4325 79TD OHWE RIKX P91', 'SAHETPGWBU', 'Chrissy Choak', 90);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (91, 'AZ26 DHKJ UX9R CKNK AOWC JJVP MVQL', 'EHRYZUFLSBO', 'Marco Baudry', 91);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (92, 'NL89 JYND 4468 6117 55', 'KONGDQLMPS', 'Hermione Skeermer', 92);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (93, 'BE87 3212 9254 8349', 'JCUQKXYNAM', 'Maximilianus Cobby', 93);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (94, 'BE65 5910 9057 7417', 'ZLIPEMNFV', 'Melina Arnfield', 94);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (95, 'FR19 2682 0999 856J IXFW RFMW Q05', 'UHGFEBXMN', 'Richart Brevetor', 95);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (96, 'LB77 5926 37QO KYMG W3JQ EWEN QAC9', 'GUQXYVIN', 'Audry Gatus', 96);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (97, 'IE28 BBWG 2014 4548 9280 23', 'AWLNPQDJBSE', 'Aymer Boyington', 97);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (98, 'PS41 PZPL B2Y3 L1XW OOU7 1XWF XGRD E', 'JMPSATDHQ', 'Staci Filipputti', 98);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (99, 'PS42 DPKL AIQS HLB0 MOR8 CAYQ XBG3 K', 'VXWHSBDO', 'Harland Lob', 99);
insert into bank_account (bank_account_id, iban, swift_code, owner_full_name, user_id) values (100, 'DK18 2334 6573 0867 54', 'TLFKROBYA', 'Andres Kleinerman', 100);

insert into connection (source_user_id, target_user_id) values (1, 54);
insert into connection (source_user_id, target_user_id) values (2, 48);
insert into connection (source_user_id, target_user_id) values (3, 81);
insert into connection (source_user_id, target_user_id) values (4, 23);
insert into connection (source_user_id, target_user_id) values (5, 55);
insert into connection (source_user_id, target_user_id) values (6, 74);
insert into connection (source_user_id, target_user_id) values (7, 75);
insert into connection (source_user_id, target_user_id) values (8, 14);
insert into connection (source_user_id, target_user_id) values (9, 27);
insert into connection (source_user_id, target_user_id) values (10, 75);
insert into connection (source_user_id, target_user_id) values (11, 92);
insert into connection (source_user_id, target_user_id) values (12, 96);
insert into connection (source_user_id, target_user_id) values (13, 88);
insert into connection (source_user_id, target_user_id) values (14, 85);
insert into connection (source_user_id, target_user_id) values (15, 97);
insert into connection (source_user_id, target_user_id) values (16, 55);
insert into connection (source_user_id, target_user_id) values (17, 45);
insert into connection (source_user_id, target_user_id) values (18, 82);
insert into connection (source_user_id, target_user_id) values (19, 96);
insert into connection (source_user_id, target_user_id) values (20, 81);
insert into connection (source_user_id, target_user_id) values (21, 27);
insert into connection (source_user_id, target_user_id) values (22, 27);
insert into connection (source_user_id, target_user_id) values (23, 77);
insert into connection (source_user_id, target_user_id) values (24, 53);
insert into connection (source_user_id, target_user_id) values (25, 20);
insert into connection (source_user_id, target_user_id) values (26, 61);
insert into connection (source_user_id, target_user_id) values (27, 60);
insert into connection (source_user_id, target_user_id) values (28, 58);
insert into connection (source_user_id, target_user_id) values (29, 41);
insert into connection (source_user_id, target_user_id) values (30, 13);
insert into connection (source_user_id, target_user_id) values (31, 18);
insert into connection (source_user_id, target_user_id) values (32, 81);
insert into connection (source_user_id, target_user_id) values (33, 74);
insert into connection (source_user_id, target_user_id) values (34, 39);
insert into connection (source_user_id, target_user_id) values (35, 59);
insert into connection (source_user_id, target_user_id) values (36, 56);
insert into connection (source_user_id, target_user_id) values (37, 72);
insert into connection (source_user_id, target_user_id) values (38, 15);
insert into connection (source_user_id, target_user_id) values (39, 1);
insert into connection (source_user_id, target_user_id) values (40, 18);
insert into connection (source_user_id, target_user_id) values (41, 94);
insert into connection (source_user_id, target_user_id) values (42, 95);
insert into connection (source_user_id, target_user_id) values (43, 19);
insert into connection (source_user_id, target_user_id) values (44, 89);
insert into connection (source_user_id, target_user_id) values (45, 23);
insert into connection (source_user_id, target_user_id) values (46, 73);
insert into connection (source_user_id, target_user_id) values (47, 70);
insert into connection (source_user_id, target_user_id) values (48, 21);
insert into connection (source_user_id, target_user_id) values (49, 19);
insert into connection (source_user_id, target_user_id) values (50, 35);
insert into connection (source_user_id, target_user_id) values (51, 91);
insert into connection (source_user_id, target_user_id) values (52, 83);
insert into connection (source_user_id, target_user_id) values (53, 16);
insert into connection (source_user_id, target_user_id) values (54, 100);
insert into connection (source_user_id, target_user_id) values (55, 98);
insert into connection (source_user_id, target_user_id) values (56, 2);
insert into connection (source_user_id, target_user_id) values (57, 57);
insert into connection (source_user_id, target_user_id) values (58, 90);
insert into connection (source_user_id, target_user_id) values (59, 75);
insert into connection (source_user_id, target_user_id) values (60, 6);
insert into connection (source_user_id, target_user_id) values (61, 50);
insert into connection (source_user_id, target_user_id) values (62, 96);
insert into connection (source_user_id, target_user_id) values (63, 30);
insert into connection (source_user_id, target_user_id) values (64, 68);
insert into connection (source_user_id, target_user_id) values (65, 41);
insert into connection (source_user_id, target_user_id) values (66, 62);
insert into connection (source_user_id, target_user_id) values (67, 83);
insert into connection (source_user_id, target_user_id) values (68, 28);
insert into connection (source_user_id, target_user_id) values (69, 89);
insert into connection (source_user_id, target_user_id) values (70, 58);
insert into connection (source_user_id, target_user_id) values (71, 35);
insert into connection (source_user_id, target_user_id) values (72, 24);
insert into connection (source_user_id, target_user_id) values (73, 12);
insert into connection (source_user_id, target_user_id) values (74, 88);
insert into connection (source_user_id, target_user_id) values (75, 15);
insert into connection (source_user_id, target_user_id) values (76, 28);
insert into connection (source_user_id, target_user_id) values (77, 77);
insert into connection (source_user_id, target_user_id) values (78, 39);
insert into connection (source_user_id, target_user_id) values (79, 21);
insert into connection (source_user_id, target_user_id) values (80, 4);
insert into connection (source_user_id, target_user_id) values (81, 70);
insert into connection (source_user_id, target_user_id) values (82, 71);
insert into connection (source_user_id, target_user_id) values (83, 84);
insert into connection (source_user_id, target_user_id) values (84, 73);
insert into connection (source_user_id, target_user_id) values (85, 27);
insert into connection (source_user_id, target_user_id) values (86, 25);
insert into connection (source_user_id, target_user_id) values (87, 42);
insert into connection (source_user_id, target_user_id) values (88, 78);
insert into connection (source_user_id, target_user_id) values (89, 27);
insert into connection (source_user_id, target_user_id) values (90, 9);
insert into connection (source_user_id, target_user_id) values (91, 27);
insert into connection (source_user_id, target_user_id) values (92, 72);
insert into connection (source_user_id, target_user_id) values (93, 27);
insert into connection (source_user_id, target_user_id) values (94, 12);
insert into connection (source_user_id, target_user_id) values (95, 37);
insert into connection (source_user_id, target_user_id) values (96, 49);
insert into connection (source_user_id, target_user_id) values (97, 91);
insert into connection (source_user_id, target_user_id) values (98, 53);
insert into connection (source_user_id, target_user_id) values (99, 77);
insert into connection (source_user_id, target_user_id) values (100, 54);

insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (1, '2021/07/13', 85.27, 'Progressive heuristic orchestration', 0.43, 29, 43, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (2, '2020/07/29', 56.56, 'Open-architected 3rd generation product', 0.28, 62, null, 52);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (3, '2021/05/14', 18.67, 'Total bandwidth-monitored hub', 0.09, 81, 72, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (4, '2020/09/27', 42.13, 'Vision-oriented foreground project', 0.21, 34, 60, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (5, '2021/05/27', 83.37, 'Implemented tertiary throughput', 0.42, 46, 59, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (6, '2020/08/15', 89.65, 'Automated encompassing architecture', 0.45, 40, 39, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (7, '2021/07/05', 19.73, 'Open-source responsive database', 0.1, 45, 1, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (8, '2021/03/31', 85.11, 'Operative maximized budgetary management', 0.43, 98, 45, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (9, '2021/03/27', 88.66, 'Realigned methodical hardware', 0.44, 50, null, 22);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (10, '2021/06/08', 94.91, 'Team-oriented responsive focus group', 0.47, 79, 40, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (11, '2020/08/29', 89.07, 'Innovative encompassing budgetary management', 0.45, 47, 8, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (12, '2021/01/13', 80.99, 'Proactive cohesive process improvement', 0.4, 36, null, 59);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (13, '2020/08/16', 47.6, 'Streamlined solution-oriented productivity', 0.24, 31, null, 67);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (14, '2020/11/24', 58.1, 'Expanded well-modulated moratorium', 0.29, 70, null, 50);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (15, '2020/08/29', 62.67, 'Multi-tiered impactful leverage', 0.31, 92, 57, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (16, '2020/08/12', 80.42, 'Vision-oriented dynamic analyzer', 0.4, 12, 23, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (17, '2020/09/14', 36.32, 'Decentralized homogeneous process improvement', 0.18, 14, 88, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (18, '2021/03/15', 76.01, 'Intuitive bifurcated product', 0.38, 8, 30, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (19, '2021/05/06', 17.24, 'Upgradable logistical architecture', 0.09, 25, 55, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (20, '2020/10/05', 39.65, 'Self-enabling systemic matrices', 0.2, 60, 51, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (21, '2021/01/22', 45.87, 'Robust static support', 0.23, 86, 62, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (22, '2021/05/07', 89.15, 'Optimized heuristic knowledge user', 0.45, 57, 12, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (23, '2020/10/28', 80.24, 'Innovative 24/7 methodology', 0.4, 88, 23, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (24, '2020/10/20', 75.46, 'Centralized homogeneous monitoring', 0.38, 69, 55, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (25, '2021/01/18', 48.26, 'Enterprise-wide 24/7 flexibility', 0.24, 50, 20, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (26, '2021/05/18', 99.98, 'Re-engineered homogeneous groupware', 0.5, 28, null, 42);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (27, '2020/11/14', 68.18, 'Reduced client-driven data-warehouse', 0.34, 49, 94, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (28, '2020/10/02', 95.32, 'Inverse value-added time-frame', 0.48, 67, 2, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (29, '2020/07/30', 35.42, 'Customizable regional process improvement', 0.18, 12, 73, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (30, '2020/09/22', 69.25, 'Customizable next generation strategy', 0.35, 36, null, 71);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (31, '2020/08/25', 40.76, 'Networked zero administration function', 0.2, 89, 8, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (32, '2021/03/01', 68.94, 'Quality-focused client-driven collaboration', 0.34, 92, 24, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (33, '2021/06/23', 76.21, 'Managed bandwidth-monitored software', 0.38, 95, null, 87);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (34, '2021/01/19', 98.53, 'Ameliorated intangible process improvement', 0.49, 2, 9, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (35, '2020/11/10', 20.76, 'Pre-emptive didactic array', 0.1, 18, 26, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (36, '2020/10/17', 14.97, 'Configurable static portal', 0.07, 26, null, 30);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (37, '2021/06/14', 27.54, 'Persevering asymmetric website', 0.14, 44, 93, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (38, '2021/04/16', 61.31, 'Cloned zero tolerance solution', 0.31, 97, 51, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (39, '2021/04/27', 37.32, 'Down-sized upward-trending strategy', 0.19, 14, null, 88);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (40, '2021/02/02', 63.21, 'Programmable local adapter', 0.32, 50, 75, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (41, '2021/03/22', 94.0, 'Diverse 3rd generation leverage', 0.47, 7, null, 73);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (42, '2021/01/08', 70.69, 'Intuitive directional infrastructure', 0.35, 67, null, 46);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (43, '2021/05/21', 55.23, 'Future-proofed systemic benchmark', 0.28, 41, 14, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (44, '2020/11/17', 79.59, 'Sharable real-time implementation', 0.4, 8, 72, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (45, '2021/04/25', 21.02, 'Total interactive system engine', 0.11, 35, 27, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (46, '2021/07/10', 70.4, 'Synergized user-facing firmware', 0.35, 73, 17, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (47, '2021/02/22', 13.43, 'Automated bandwidth-monitored attitude', 0.07, 37, 42, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (48, '2020/10/21', 17.66, 'Progressive incremental artificial intelligence', 0.09, 90, 60, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (49, '2021/01/08', 41.92, 'Customer-focused leading edge customer loyalty', 0.21, 49, 82, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (50, '2020/12/15', 95.63, 'Open-source composite support', 0.48, 50, 12, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (51, '2021/07/09', 74.59, 'Right-sized interactive utilisation', 0.37, 44, null, 69);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (52, '2021/05/17', 21.92, 'Organic modular support', 0.11, 94, null, 63);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (53, '2020/08/07', 80.28, 'Advanced fault-tolerant extranet', 0.4, 46, 99, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (54, '2020/09/03', 90.95, 'Diverse intermediate hierarchy', 0.45, 58, 12, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (55, '2021/01/20', 58.21, 'Grass-roots impactful middleware', 0.29, 13, 88, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (56, '2021/02/13', 99.29, 'Diverse client-server concept', 0.5, 14, null, 12);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (57, '2020/09/10', 32.43, 'Virtual optimal concept', 0.16, 74, 36, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (58, '2021/01/19', 10.72, 'Triple-buffered next generation capability', 0.05, 92, 43, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (59, '2020/12/09', 47.2, 'Synergistic interactive system engine', 0.24, 94, 67, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (60, '2021/04/11', 20.24, 'Implemented static core', 0.1, 59, null, 31);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (61, '2020/08/04', 50.76, 'Advanced tertiary model', 0.25, 63, 77, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (62, '2020/11/28', 86.42, 'Open-source tertiary intranet', 0.43, 33, 60, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (63, '2021/04/05', 85.25, 'Public-key clear-thinking ability', 0.43, 68, 8, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (64, '2021/04/04', 64.13, 'Optimized high-level matrices', 0.32, 76, null, 16);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (65, '2021/07/18', 75.91, 'Grass-roots 6th generation portal', 0.38, 68, null, 14);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (66, '2021/03/16', 92.58, 'Triple-buffered multi-tasking installation', 0.46, 72, 56, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (67, '2021/04/19', 24.17, 'Secured 24/7 secured line', 0.12, 75, null, 3);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (68, '2020/09/23', 73.52, 'Reverse-engineered logistical instruction set', 0.37, 84, 29, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (69, '2020/09/23', 83.43, 'Optional bandwidth-monitored info-mediaries', 0.42, 83, 36, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (70, '2021/04/01', 78.45, 'Monitored incremental encoding', 0.39, 86, null, 81);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (71, '2020/08/01', 24.45, 'Programmable cohesive superstructure', 0.12, 45, 9, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (72, '2021/04/19', 30.0, 'Multi-lateral clear-thinking budgetary management', 0.15, 83, 80, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (73, '2021/01/31', 73.18, 'Right-sized human-resource groupware', 0.37, 31, 43, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (74, '2020/10/13', 30.91, 'Advanced impactful capability', 0.15, 66, null, 95);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (75, '2020/12/28', 70.92, 'Organized dedicated knowledge base', 0.35, 58, 54, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (76, '2021/05/08', 50.41, 'User-centric eco-centric contingency', 0.25, 58, 86, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (77, '2020/12/18', 64.67, 'Cloned explicit function', 0.32, 81, null, 30);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (78, '2021/04/08', 68.13, 'Profit-focused value-added neural-net', 0.34, 11, 73, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (79, '2020/07/31', 60.35, 'Mandatory cohesive complexity', 0.3, 12, 44, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (80, '2021/06/01', 53.44, 'Total object-oriented middleware', 0.27, 24, 73, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (81, '2021/02/14', 22.75, 'Virtual motivating help-desk', 0.11, 23, 4, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (82, '2020/08/19', 42.15, 'Secured systemic artificial intelligence', 0.21, 99, 37, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (83, '2020/10/03', 24.14, 'Multi-lateral hybrid help-desk', 0.12, 59, 100, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (84, '2020/11/02', 89.19, 'Expanded composite algorithm', 0.45, 26, 13, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (85, '2021/07/18', 19.15, 'Customer-focused homogeneous neural-net', 0.1, 35, null, 33);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (86, '2021/02/10', 86.27, 'Centralized scalable core', 0.43, 28, 29, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (87, '2020/08/28', 60.86, 'Organic reciprocal workforce', 0.3, 67, null, 55);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (88, '2020/10/16', 98.35, 'Multi-lateral bifurcated initiative', 0.49, 29, 36, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (89, '2021/01/26', 24.41, 'Virtual upward-trending alliance', 0.12, 67, 45, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (90, '2021/04/09', 83.15, 'Universal multi-state budgetary management', 0.42, 82, 12, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (91, '2021/05/30', 72.61, 'Business-focused intangible projection', 0.36, 77, 44, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (92, '2021/06/26', 55.42, 'Cross-platform content-based open system', 0.28, 65, null, 22);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (93, '2020/11/01', 14.49, 'Decentralized clear-thinking flexibility', 0.07, 36, 19, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (94, '2021/04/16', 90.56, 'Business-focused high-level open system', 0.45, 36, null, 46);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (95, '2021/01/05', 87.48, 'Expanded value-added framework', 0.44, 17, 69, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (96, '2021/05/03', 69.4, 'Down-sized regional software', 0.35, 43, 36, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (97, '2021/04/17', 58.29, 'Compatible user-facing focus group', 0.29, 32, 23, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (98, '2020/10/06', 98.26, 'Horizontal web-enabled migration', 0.49, 3, 12, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (99, '2021/05/02', 54.49, 'Cloned mobile archive', 0.27, 33, 2, null);
insert into transaction (transaction_id, date, amount, description, commission_amount, sender_wallet_id, receiver_wallet_id, receiver_bank_account_id) values (100, '2021/03/31', 85.51, 'Reactive contextually-based middleware', 0.43, 12, null, 41);

COMMIT;
