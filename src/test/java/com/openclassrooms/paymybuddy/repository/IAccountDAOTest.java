package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static com.openclassrooms.paymybuddy.constants.SecurityConstants.USER_NOT_FOUND_MSG;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IAccountDAOTest {

	@Autowired
	IAccountDAO iAccountDAO;

	@Autowired
	IUserDAO iUserDAO;

	private User user;

	private Account account;

	@Test
	void testSavingAnAccount_shouldReturnTheSavedAccount_forAValidAccount() {
		User addedUser = iUserDAO.save(user);
		assertThat(addedUser).isNotNull();
		Account savedAccount = iAccountDAO.save(account);
		assertThat(savedAccount.getEmail()).isEqualTo("eeee@il.fr");
	}

	@Test
	void testRetrieveAnAccountByEmail_shouldReturnTheAccount_forAValidEmail() {
		iUserDAO.save(user);
		var account = iAccountDAO.getByEmail("eeee@il.fr");
		account.orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, "eeee@il.fr")));
		assertThat(account.get().getPassword()).isEqualTo(
				"password");
	}

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Ali");
		user.setLastName("Baba");
		account = new Account();
		user.setAccount(account);
		account.setEmail("eeee@il.fr");
		account.setPassword("password");
	}
}
