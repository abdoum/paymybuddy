package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.Connection;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IConnectionDAOTest {

	@Autowired
	IConnectionDAO iConnectionDAO;

	@Autowired
	IUserDAO iUserDAO;

	private User user;

	private User user2;

	private Connection connection;

	private Account account;

	private Account account2;

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Ali");
		user.setLastName("Baba");

		user2 = new User();
		user2.setFirstName("Holy");
		user2.setLastName("Molly");

		account = new Account();
		account.setEmail("ed@fe.fr");
		account.setRoles(Roles.USER);
		account.setPassword("aaaaargh");

		user.setAccount(account);

		connection = new Connection();
		connection.setSourceUser(user2);
		connection.setTargetUser(user);

		account2 = new Account();
		account2.setEmail("aaaaaad@fe.fr");
		account2.setRoles(Roles.USER);
		account2.setPassword("aaaaargh");

		user2.setAccount(account2);
	}

	@Test
	void testSaveAConnection_shouldReturnTheSavedConnection_forTwoExistingUsers() {
		iUserDAO.save(user);
		iUserDAO.save(user2);
		var addedConnection = iConnectionDAO.save(connection);
		assertThat(addedConnection.getSourceUser().getFirstName()).isEqualTo("Holy");
		assertThat(addedConnection.getTargetUser()).isNotNull();
	}
}
