package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Currency;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ITransactionDAOTest {

	@Mock
	Connection connection;

	@Autowired
	private IUserDAO iUserDAO;

	@Autowired
	private IAccountDAO iAccountDAO;

	@Autowired
	private IWalletDAO iWalletDAO;

	@Autowired
	private ITransactionDAO iTransactionDAO;

	@Autowired
	private IConnectionDAO iConnectionDAO;

	private Account account2;

	private Account account;

	private Wallet wallet;

	private Wallet wallet2;

	private Transaction transaction;

	private User user;

	private User user2;

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Holy");
		user.setLastName("Molly");

		user2 = new User();
		user2.setFirstName("Ali");
		user2.setLastName("Baba");

		account = new Account();
		user.setAccount(account);
		account.setEmail("etttaaa@il.fr");
		account.setPassword("password");

		account2 = new Account();
		user2.setAccount(account2);
		account2.setEmail("errraaa@il.fr");
		account2.setPassword("passwordaa");

		Currency currency = Currency.getInstance(Locale.JAPAN);
		wallet = new Wallet();
		wallet.setBalance(190.d);
		wallet.setCurrency(currency);

		wallet2 = new Wallet();
		wallet2.setBalance(500.d);
		wallet2.setCurrency(currency);
		wallet.setUser(user);
		wallet2.setUser(user2);

		transaction = new Transaction();
		transaction.setAmount(90.);
		transaction.setDescription("restaurant bill");
		transaction.setSenderWallet(wallet);
		connection = new Connection();
		connection.setSourceUser(user);
		connection.setTargetUser(user2);
	}

	@Test
	void testSaveATransaction_shouldReturnTheSavedTransaction_forAnExistingUserWithAWalletAndAConnection() throws
			EmptySearchResult, BadArgument {
		iConnectionDAO.save(connection);
		iAccountDAO.save(account);
		iAccountDAO.save(account2);
		User savedUser0 = iUserDAO.save(user);
		User savedUser1 = iUserDAO.save(user2);
		savedUser0.setAccount(account);
		savedUser1.setAccount(account2);
		iWalletDAO.save(wallet);
		iWalletDAO.save(wallet2);

		var foundConnections = iConnectionDAO.getBySourceUser(savedUser0).stream().findFirst().orElseThrow(() ->
				new EmptySearchResult("no user found"));
		;
		var foundWallet =
				iWalletDAO.findByUserId(foundConnections.getTargetUser().getId())
						.orElseThrow(() -> new BadArgument("wallet not " +
								"found"));
		transaction.setReceiverWallet(foundWallet);

		Transaction savedTransaction = iTransactionDAO.save(transaction);
		assertThat(savedTransaction.getDescription()).isEqualTo("restaurant bill");
	}
}
