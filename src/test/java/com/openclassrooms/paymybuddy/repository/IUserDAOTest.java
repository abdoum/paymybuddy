package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IUserDAOTest {

	@Autowired
	IUserDAO iUserDAO;

	private User user;

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Ali");
		user.setLastName("Baba");

		Account account = new Account();
		account.setEmail("ed@fe.fr");
		account.setRoles(Roles.USER);
		account.setPassword("aaaaargh");

		user.setAccount(account);
	}

	@Test
	void testSaveUser_shouldReturnTheSavedUser_forAValidUser() {
		User addedUser = iUserDAO.save(user);
		assertThat(addedUser.getFirstName()).isEqualTo("Ali");
		assertThat(addedUser.getId()).isNotNull();
	}

	@Test
	void testRetrieveSavedUsers_shouldReturnAListOfUsers_forATableContainingUsers() {
		user.setLastName("Another");
		User addedUser0 = iUserDAO.save(user);
		List<User> savedUsers = iUserDAO.findAll();
		assertThat(savedUsers).contains(addedUser0);
	}

	@Test
	void testDeleteUser_shouldRunWithoutExceptions_forAnExistingUser() {
		User addedUser0 = iUserDAO.save(user);
		iUserDAO.deleteById(addedUser0.getId());
		List<User> savedUsers = iUserDAO.findAll();
		assertThat(savedUsers)
				.isNotEmpty()
				.doesNotContain(user);
	}

	@Test
	void testUpdateUser_shouldReturnTheUpdatedUser_forAnExistingUser() throws BadArgument {
		User addedUser0 = iUserDAO.save(user);
		user.setLastName("modified LastName");  // the name will automatically be converted to titleCase
		user.setId(addedUser0.getId());
		var updatedUser = iUserDAO.save(user);
		var userAfterUpdate = iUserDAO.findById(updatedUser.getId()).orElseThrow(() -> new BadArgument("user not " +
				"found"));
		assertThat(userAfterUpdate.getLastName()).isEqualTo("Modified Lastname");
	}
}
