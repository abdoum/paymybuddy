package com.openclassrooms.paymybuddy.repository;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.BankAccount;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.model.Wallet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.util.Currency;
import java.util.Locale;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class IWalletDAOTest {

	@Autowired
	IWalletDAO iWalletDAO;

	@Autowired
	IUserDAO iUserDAO;

	@Autowired
	IAccountDAO iAccountDAO;

	@Autowired
	IBankAccountDAO iBankAccountDAO;

	private User user;

	private Account account;

	private Wallet wallet;

	private BankAccount bankAccount;

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Ali");
		user.setLastName("Baba");
		account = new Account();
		user.setAccount(account);
		account.setEmail("opp@il.fr");
		account.setPassword("password");

		bankAccount = new BankAccount();
		bankAccount.setIban("CY57 2498 0517 QPOA L1IZ YRNC UIFJ");
		bankAccount.setSwiftCode("CNKRUBXTDP");
		bankAccount.setOwnerFullName(user.getFirstName() + " " + user.getLastName());
		bankAccount.setUser(user);

		Currency currency = Currency.getInstance(Locale.FRANCE);
		wallet = new Wallet();
		wallet.setBalance(190.d);
		wallet.setCurrency(currency);
		wallet.setUser(user);
	}

	@Test
	@Transactional
	void testSavingAWallet_shouldReturnTheSavedWallet_forAValidUserAndAccount() {
		Account savedAccount = iAccountDAO.save(account);
		User savedUser = iUserDAO.save(user);
		bankAccount.setUser(user);
		bankAccount = iBankAccountDAO.save(bankAccount);
		Wallet savedWallet = iWalletDAO.save(wallet);
		assertThat(savedWallet.getBalance()).isEqualTo(190.d);
	}

	@Test
	void testUpdatingAWallet_shouldReturnTheUpdatedWallet_forAValidUserAndAccount() {
		account.setEmail("anotheremail@em.de");
		Account savedAccount = iAccountDAO.save(account);
		User savedUser = iUserDAO.save(user);
		Wallet savedWallet = iWalletDAO.save(wallet);
		wallet.setBalance(800.);
		Wallet updatedWallet = iWalletDAO.save(wallet);
		assertThat(updatedWallet.getBalance()).isEqualTo(800.);
	}

	@Test
	void testUpdateBalance() {
		Optional<Wallet> savedWallet = iWalletDAO.findById(3L);
		if (savedWallet.isPresent()) {
			var walletToUpdate = savedWallet.get();
			assertThat(walletToUpdate.getBalance()).isEqualTo(190.0);
			walletToUpdate.setBalance(walletToUpdate.getBalance() - 5.);
			iWalletDAO.save(walletToUpdate);
			assertThat(walletToUpdate.getBalance()).isEqualTo(185.);
		}
	}
}
