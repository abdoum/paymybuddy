package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.*;
import com.openclassrooms.paymybuddy.repository.IAccountDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AccountService.class})
@ExtendWith(SpringExtension.class)
class AccountServiceTest {

	@Autowired
	AccountService accountService;

	@MockBean
	private IAuthService iAuthService;

	@MockBean
	private IBankAccountService iBankAccountService;

	@MockBean
	private IUserService iUserService;

	@MockBean
	private IWalletService iWalletService;

	@MockBean
	private IAccountDAO iAccountDAO;

	@MockBean
	private PasswordEncoder passwordEncoder;

	private Account account;

	private User user;

	private BankAccount bankAccount;

	private SignUpRequest signUpRequest;

	private Wallet wallet;

	@BeforeEach
	void setUp() {
		user = new User();
		user.setFirstName("Ali");
		user.setLastName("Baba");

		account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(false);
		account.setCredentialExpired(false);
		account.setAccountLocked(false);

		wallet = new Wallet();
		wallet.setCurrency(null);
		wallet.setBalance(10.0);
		wallet.setUser(user);
		wallet.setId(123L);

		bankAccount = new BankAccount();
		bankAccount.setIban("CY57 2498 0517 QPOA L1IZ YRNC UIFJ");
		bankAccount.setSwiftCode("CNKRUBXTDP");
		bankAccount.setOwnerFullName(user.getFirstName() + " " + user.getLastName());
		bankAccount.setUser(user);

		signUpRequest = new SignUpRequest();
		signUpRequest.setFirstName(user.getFirstName());
		signUpRequest.setLastName(user.getLastName());
		signUpRequest.setUsername(account.getEmail());
		signUpRequest.setBankAccount(bankAccount);
		signUpRequest.setPassword(account.getPassword());
		signUpRequest.setPasswordConfirmation(account.getPassword());
	}

	@Test
	void testFindOneByEmail_shouldReturnTheFoundAccount_forAValidAccountEmail() throws BadArgument {
		Optional<Account> ofResult = Optional.<Account>of(account);
		when(this.iAccountDAO.findByEmail(anyString())).thenReturn(ofResult);
		var actualFindByEmailResult = this.accountService.findOneByEmail("jane.doe@example.org");
		assertSame(ofResult, actualFindByEmailResult);
		assertNotNull(actualFindByEmailResult);
		verify(this.iAccountDAO).findByEmail(anyString());
	}

	@Test
	void testInitializeServices_shouldThrowABadArgumentException_forAnExistingAccountWithTheSameUsername() throws
			BadArgument,
			EmptySearchResult {
		Optional<Account> ofResult = Optional.<Account>of(account);
		when(this.iAccountDAO.findByEmail((String) any())).thenReturn(ofResult);
		assertThrows(BadArgument.class, () -> this.accountService.initializeServices(signUpRequest));
		verify(this.iAccountDAO).findByEmail((String) any());
	}

	@Test
	void testInitializeServices_shouldReturnAJwtRespose_forAValidSignUpRequest() throws BadArgument, EmptySearchResult {
		when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
		when(this.iWalletService.save((User) any())).thenReturn(wallet);
		when(this.iUserService.save((SignUpRequest) any(), (Account) any())).thenReturn(user);
		when(this.iBankAccountService.save((BankAccount) any())).thenReturn(bankAccount);
		JwtResponse jwtResponse = new JwtResponse("ABC123", 123L, "janedoe", "jane.doe@example.org",
				new ArrayList<GrantedAuthority>());
		when(this.iAuthService.authenticate((String) any(), (String) any())).thenReturn(jwtResponse);
		when(this.iAccountDAO.save((Account) any())).thenReturn(account);
		when(this.iAccountDAO.findByEmail((String) any())).thenReturn(Optional.<Account>empty());

		assertSame(jwtResponse, this.accountService.initializeServices(signUpRequest));
		verify(this.passwordEncoder).encode((CharSequence) any());
		verify(this.iWalletService).save((User) any());
		verify(this.iUserService).save((SignUpRequest) any(), (Account) any());
		verify(this.iBankAccountService).save((BankAccount) any());
		verify(this.iAuthService).authenticate((String) any(), (String) any());
		verify(this.iAccountDAO).findByEmail((String) any());
		verify(this.iAccountDAO).save((Account) any());
	}

	@Test
	void testSave_shouldReturnTheSavedAccount_forAValidAccountAndANonExistingUsername() throws BadArgument {
		when(this.iAccountDAO.save((Account) any())).thenReturn(account);
		assertSame(account, this.accountService.save(account));
		verify(this.iAccountDAO).save((Account) any());
		assertEquals("iloveyou", account.getPassword());
	}

	@Test
	void testSave_shouldThrowABadArgumentException_forAnAlreadyExistingAccountWithTheSameEmail() throws BadArgument {
		Optional<Account> ofResult = Optional.<Account>of(account);
		when(this.iAccountDAO.findByEmail((String) any())).thenReturn(ofResult);
		assertThrows(BadArgument.class, () -> this.accountService.save(signUpRequest));
		verify(this.iAccountDAO).findByEmail((String) any());
	}

	@Test
	void testSave_shouldReturnTheSavedAccount_forANonExistingUsername() throws BadArgument {
		when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
		when(this.iAccountDAO.save((Account) any())).thenReturn(account);
		when(this.iAccountDAO.findByEmail((String) any())).thenReturn(Optional.<Account>empty());
		assertSame(account, this.accountService.save(signUpRequest));
		verify(this.passwordEncoder).encode((CharSequence) any());
		verify(this.iAccountDAO).findByEmail((String) any());
		verify(this.iAccountDAO).save((Account) any());
	}

	@Test
	void testGetByEmail_shouldReturnTheRequestedAccount_forAnExistingEmail() {
		Optional<Account> ofResult = Optional.<Account>of(account);
		when(this.iAccountDAO.getByEmail((String) any())).thenReturn(ofResult);
		Optional<Account> actualByEmail = this.accountService.getByEmail("jane.doe@example.org");
		assertSame(ofResult, actualByEmail);
		assertTrue(actualByEmail.isPresent());
		verify(this.iAccountDAO).getByEmail((String) any());
	}

	@Test
	void testLoadUserByUsername_shouldReturnTheFoundAccount_forAnExistingUsername() throws UsernameNotFoundException {
		Optional<Account> ofResult = Optional.<Account>of(account);
		when(this.iAccountDAO.getByEmail(anyString())).thenReturn(ofResult);
		assertSame(account, this.accountService.loadUserByUsername("janedoe"));
		verify(this.iAccountDAO).getByEmail(anyString());
	}

	@Test
	void testLoadUserByUsername_shouldThrowAUserNotFoundException_forAnNonExistingUsername() throws
			UsernameNotFoundException {
		when(this.iAccountDAO.getByEmail(anyString())).thenReturn(Optional.<Account>empty());
		assertThrows(UsernameNotFoundException.class, () -> this.accountService.loadUserByUsername("janedoe"));
		verify(this.iAccountDAO).getByEmail(anyString());
	}
}
