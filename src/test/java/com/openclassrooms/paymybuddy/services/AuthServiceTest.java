package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.config.jwt.JwtUtils;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.JwtResponse;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AuthService.class, JwtUtils.class})
@ExtendWith(SpringExtension.class)
class AuthServiceTest {

	@Autowired
	private AuthService authService;

	@MockBean
	private AuthenticationManager authenticationManager;

	@MockBean
	private IUserService iUserService;

	@MockBean
	private JwtUtils jwtUtils;

	@BeforeEach
	void setUp() {
	}

	@Test
	public void testAuthenticate() throws EmptySearchResult, AuthenticationException {
		when(this.jwtUtils.generateJwtToken((Authentication) any())).thenReturn("foo");

		Account account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);

		User user = new User();
		user.setLastName("Doe");
		user.setId(123L);
		user.setAccount(account);
		user.setFirstName("Jane");
		Optional<User> ofResult = Optional.<User>of(user);
		when(this.iUserService.findOneByAccount((Account) any())).thenReturn(ofResult);

		Account account1 = new Account();
		account1.setEmail("jane.doe@example.org");
		account1.setPassword("iloveyou");
		account1.setId(123L);
		account1.setRoles(Roles.USER);
		account1.setAccountEnabled(true);
		account1.setAccountExpired(true);
		account1.setCredentialExpired(true);
		account1.setAccountLocked(true);
		TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(account1, "Credentials");

		when(this.authenticationManager.authenticate((Authentication) any())).thenReturn(testingAuthenticationToken);
		JwtResponse actualAuthenticateResult = this.authService.authenticate("janedoe", "iloveyou");
		assertEquals("foo", actualAuthenticateResult.getAccessToken());
		assertEquals("jane.doe@example.org", actualAuthenticateResult.getUsername());
		assertEquals("Bearer", actualAuthenticateResult.getTokenType());
		Collection<? extends GrantedAuthority> roles = actualAuthenticateResult.getRoles();
		assertEquals(1, roles.size());
		assertEquals(123L, actualAuthenticateResult.getId().longValue());
		assertEquals("jane.doe@example.org", actualAuthenticateResult.getEmail());
		assertEquals("USER", ((List<? extends GrantedAuthority>) roles).get(0).getAuthority());
		verify(this.jwtUtils).generateJwtToken((Authentication) any());
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.authenticationManager).authenticate((Authentication) any());
	}

	@Test
	void testAuthenticate2() throws EmptySearchResult, AuthenticationException {
		when(this.jwtUtils.generateJwtToken((Authentication) any())).thenReturn("foo");

		Account account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);

		User user = new User();
		user.setLastName("Doe");
		user.setId(123L);
		user.setAccount(account);
		user.setFirstName("Jane");
		Optional<User> ofResult = Optional.<User>of(user);
		when(this.iUserService.findOneByAccount((Account) any())).thenReturn(ofResult);

		Account account1 = new Account();
		account1.setEmail("jane.doe@example.org");
		account1.setPassword("iloveyou");
		account1.setId(123L);
		account1.setRoles(Roles.USER);
		account1.setAccountEnabled(true);
		account1.setAccountExpired(true);
		account1.setCredentialExpired(true);
		account1.setAccountLocked(true);
		TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(account1, "Credentials");

		when(this.authenticationManager.authenticate((Authentication) any())).thenReturn(testingAuthenticationToken);
		JwtResponse actualAuthenticateResult = this.authService.authenticate("janedoe", "iloveyou");
		assertEquals("foo", actualAuthenticateResult.getAccessToken());
		assertEquals("jane.doe@example.org", actualAuthenticateResult.getUsername());
		assertEquals("Bearer", actualAuthenticateResult.getTokenType());
		Collection<? extends GrantedAuthority> roles = actualAuthenticateResult.getRoles();
		assertEquals(1, roles.size());
		assertEquals(123L, actualAuthenticateResult.getId().longValue());
		assertEquals("jane.doe@example.org", actualAuthenticateResult.getEmail());
		assertEquals("USER", ((List<? extends GrantedAuthority>) roles).get(0).getAuthority());
		verify(this.jwtUtils).generateJwtToken((Authentication) any());
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.authenticationManager).authenticate((Authentication) any());
	}

	@Test
	void testAuthenticate3() throws EmptySearchResult, AuthenticationException {
		when(this.jwtUtils.generateJwtToken((Authentication) any())).thenReturn("foo");
		when(this.iUserService.findOneByAccount((Account) any())).thenReturn(Optional.<User>empty());

		Account account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);
		TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(account, "Credentials");

		when(this.authenticationManager.authenticate((Authentication) any())).thenReturn(testingAuthenticationToken);
		assertThrows(EmptySearchResult.class, () -> this.authService.authenticate("janedoe", "iloveyou"));
		verify(this.jwtUtils).generateJwtToken((Authentication) any());
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.authenticationManager).authenticate((Authentication) any());
	}

	@Test
	void testAuthenticate4() throws EmptySearchResult, AuthenticationException {
		when(this.jwtUtils.generateJwtToken((Authentication) any())).thenReturn("foo");
		when(this.iUserService.findOneByAccount((Account) any())).thenReturn(Optional.<User>empty());

		Account account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);
		TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(account, "Credentials");

		when(this.authenticationManager.authenticate((Authentication) any())).thenReturn(testingAuthenticationToken);
		assertThrows(EmptySearchResult.class, () -> this.authService.authenticate("janedoe", "iloveyou"));
		verify(this.jwtUtils).generateJwtToken((Authentication) any());
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.authenticationManager).authenticate((Authentication) any());
	}
}
