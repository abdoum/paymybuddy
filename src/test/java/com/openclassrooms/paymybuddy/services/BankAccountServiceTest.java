package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.BankAccount;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.repository.IBankAccountDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {BankAccountService.class})
@ExtendWith(SpringExtension.class)
class BankAccountServiceTest {

	Account account = new Account();

	User user = new User();

	BankAccount bankAccount = new BankAccount();

	Account account1 = new Account();

	User user1 = new User();

	BankAccount bankAccount1 = new BankAccount();

	@Autowired
	private BankAccountService bankAccountService;

	@MockBean
	private IBankAccountDAO iBankAccountDAO;

	@BeforeEach
	void setUp() {
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);

		user.setLastName("Doe");
		user.setId(123L);
		user.setAccount(account);
		user.setFirstName("Jane");

		bankAccount.setSwiftCode("Swift Code");
		bankAccount.setUser(user);
		bankAccount.setId(3L);
		bankAccount.setOwnerFullName("Dr Jane Doe");
		bankAccount.setIban("Iban");

		account1.setEmail("jane.doe@example.org");
		account1.setPassword("iloveyou");
		account1.setId(123L);
		account1.setRoles(Roles.USER);
		account1.setAccountEnabled(true);
		account1.setAccountExpired(true);
		account1.setCredentialExpired(true);
		account1.setAccountLocked(true);

		user1.setLastName("Doe");
		user1.setId(123L);
		user1.setAccount(account1);
		user1.setFirstName("Jane");

		bankAccount1.setSwiftCode("Swift Code");
		bankAccount1.setUser(user1);
		bankAccount1.setId(123L);
		bankAccount1.setOwnerFullName("Dr Jane Doe");
		bankAccount1.setIban("Iban");
	}

	@Test
	void testAdd_shouldReturnTheAddedBankAccount_forAValidBankAccount() {
		when(this.iBankAccountDAO.save((BankAccount) any())).thenReturn(bankAccount);
		assertSame(bankAccount, this.bankAccountService.save(bankAccount1));
		verify(this.iBankAccountDAO).save((BankAccount) any());
	}

	@Test
	void testGetOneByUserId_shouldReturnTheFoundBankAccount_forAnExistingBankAccount() throws EmptySearchResult {
		Optional<BankAccount> ofResult = Optional.<BankAccount>of(bankAccount);
		when(this.iBankAccountDAO.findByUserId((Long) any())).thenReturn(ofResult);
		assertSame(bankAccount, this.bankAccountService.getOneByUserId(123L));
		verify(this.iBankAccountDAO).findByUserId((Long) any());
	}

	@Test
	void testGetOneByUserId_shouldThrowEmptySearchResultException_forANonExistingBankAccount() throws
			EmptySearchResult {
		when(this.iBankAccountDAO.findByUserId((Long) any())).thenReturn(Optional.<BankAccount>empty());
		assertThrows(EmptySearchResult.class, () -> this.bankAccountService.getOneByUserId(123L));
		verify(this.iBankAccountDAO).findByUserId((Long) any());
	}
}
