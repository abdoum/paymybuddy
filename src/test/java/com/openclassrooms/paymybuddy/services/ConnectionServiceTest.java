package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.Connection;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.repository.IConnectionDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ConnectionService.class})
@ExtendWith(SpringExtension.class)
class ConnectionServiceTest {

	Account account;

	Account account1;

	Account account2;

	Account account3;

	User user;

	User user1;

	User user2;

	Connection connection = new Connection();

	@Autowired
	ConnectionService connectionService;

	@MockBean
	IAccountService iAccountService;

	@MockBean
	IConnectionDAO iConnectionDAO;

	@MockBean
	IUserService iUserService;

	@BeforeEach
	void setUp() {

		account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(false);
		account.setCredentialExpired(false);
		account.setAccountLocked(false);

		user = new User();
		user.setLastName("Doe");
		user.setId(123L);
		user.setAccount(account);
		user.setFirstName("Jane");

		account1 = new Account();
		account1.setEmail("jane.doe@example.org");
		account1.setPassword("iloveyou");
		account1.setId(123L);
		account1.setRoles(Roles.USER);
		account1.setAccountEnabled(true);
		account1.setAccountExpired(false);
		account1.setCredentialExpired(false);
		account1.setAccountLocked(false);

		user1 = new User();
		user1.setLastName("Doe");
		user1.setId(123L);
		user1.setAccount(account1);
		user1.setFirstName("Jane");

		account2 = new Account();
		account2.setEmail("jane.doe@example.org");
		account2.setPassword("iloveyou");
		account2.setId(123L);
		account2.setRoles(Roles.USER);
		account2.setAccountEnabled(true);
		account2.setAccountExpired(false);
		account2.setCredentialExpired(false);
		account2.setAccountLocked(false);

		user2 = new User();
		user2.setLastName("Doe");
		user2.setId(123L);
		user2.setAccount(account2);
		user2.setFirstName("Jane");

		connection.setSourceUser(user1);
		connection.setId(123L);
		connection.setTargetUser(user2);
	}

	@Test
	void testAdd() throws BadArgument {
		Optional<User> ofResult = Optional.<User>of(user);
		when(this.iUserService.findOneById((Long) any())).thenReturn(ofResult);

		Optional<Connection> ofResult1 = Optional.<Connection>of(connection);
		when(this.iConnectionDAO.findOneBySourceUserIdAndTargetUser((Long) any(), (String) any())).thenReturn(
				ofResult1);
		assertThrows(BadArgument.class, () -> this.connectionService.add(123L, "jane.doe@example.org"));
		verify(this.iUserService).findOneById((Long) any());
		verify(this.iConnectionDAO).findOneBySourceUserIdAndTargetUser((Long) any(), (String) any());
	}

	@Test
	void testAdd2() throws BadArgument {

		when(this.iConnectionDAO.save((Connection) any())).thenReturn(connection);
		when(this.iConnectionDAO.findOneBySourceUserIdAndTargetUser((Long) any(), (String) any()))
				.thenReturn(Optional.<Connection>empty());

		Account account4 = new Account();
		account4.setEmail("jane.doe@example.org");
		account4.setPassword("iloveyou");
		account4.setId(123L);
		account4.setRoles(Roles.USER);
		account4.setAccountEnabled(true);
		account4.setAccountExpired(false);
		account4.setCredentialExpired(false);
		account4.setAccountLocked(false);
		Optional<Account> ofResult2 = Optional.<Account>of(account4);
		when(this.iAccountService.findOneByEmail((String) any())).thenReturn(ofResult2);
		assertSame(connection, this.connectionService.add(123L, "jane.doe@example.org"));
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.iUserService).findOneById((Long) any());
		verify(this.iConnectionDAO).findOneBySourceUserIdAndTargetUser((Long) any(), (String) any());
		verify(this.iConnectionDAO).save((Connection) any());
		verify(this.iAccountService).findOneByEmail((String) any());
	}

	@Test
	void testAdd3() throws BadArgument {

		Optional<User> ofResult = Optional.<User>of(user);
		when(this.iUserService.findOneByAccount((Account) any())).thenReturn(Optional.<User>empty());
		when(this.iUserService.findOneById((Long) any())).thenReturn(ofResult);

		when(this.iConnectionDAO.save((Connection) any())).thenReturn(connection);
		when(this.iConnectionDAO.findOneBySourceUserIdAndTargetUser((Long) any(), (String) any()))
				.thenReturn(Optional.<Connection>empty());

		Optional<Account> ofResult1 = Optional.<Account>of(account1);
		when(this.iAccountService.findOneByEmail((String) any())).thenReturn(ofResult1);
		assertThrows(BadArgument.class, () -> this.connectionService.add(123L, "jane.doe@example.org"));
		verify(this.iUserService).findOneByAccount((Account) any());
		verify(this.iUserService).findOneById((Long) any());
		verify(this.iConnectionDAO).findOneBySourceUserIdAndTargetUser((Long) any(), (String) any());
		verify(this.iAccountService).findOneByEmail((String) any());
	}

	@Test
	void testGetConnectionsCountBySourceUserId() {
		when(this.iConnectionDAO.getConnectionsCountBySourceUserId((Long) any())).thenReturn(Optional.<Double>of(10.0));
		assertEquals(10.0, this.connectionService.getConnectionsCountBySourceUserId(123L).doubleValue());
		verify(this.iConnectionDAO).getConnectionsCountBySourceUserId((Long) any());
	}
}
