package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.exception.BadArgument;
import com.openclassrooms.paymybuddy.exception.EmptySearchResult;
import com.openclassrooms.paymybuddy.model.*;
import com.openclassrooms.paymybuddy.repository.ITransactionDAO;
import com.openclassrooms.paymybuddy.repository.IWalletDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TransactionService.class})
@ExtendWith(SpringExtension.class)
//@RunWith(SpringRunner.class)
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TransactionServiceTest {

	@Autowired
	TransactionService transactionService;

	@MockBean
	IWalletDAO iWalletDAO;

	@MockBean
	private IBankAccountService iBankAccountService;

	@MockBean
	private IWalletService iWalletService;

	@MockBean
	private ITransactionDAO iTransactionDAO;

	private TransactionRequest transactionRequest;

	private Wallet senderWallet;

	private Wallet receiverWallet;

	private BankAccount receiverBankAccount;

	private Transaction transaction;

	@BeforeEach
	void setUp() {
		Account senderAccount = new Account();
		senderAccount.setEmail("jane.doe@example.org");
		senderAccount.setPassword("iloveyou");
		senderAccount.setId(123L);
		senderAccount.setRoles(Roles.USER);
		senderAccount.setAccountEnabled(true);
		senderAccount.setAccountExpired(false);
		senderAccount.setCredentialExpired(false);
		senderAccount.setAccountLocked(false);

		User senderUser = new User();
		senderUser.setLastName("Doe");
		senderUser.setId(123L);
		senderUser.setAccount(senderAccount);
		senderUser.setFirstName("Jane");

		senderWallet = new Wallet();
		senderWallet.setCurrency(null);
		senderWallet.setBalance(10.0);
		senderWallet.setUser(senderUser);
		senderWallet.setId(123L);

		Account receiverAccount = new Account();
		receiverAccount.setEmail("jane.doe@example.org");
		receiverAccount.setPassword("iloveyou");
		receiverAccount.setId(123L);
		receiverAccount.setRoles(Roles.USER);
		receiverAccount.setAccountEnabled(true);
		receiverAccount.setAccountExpired(false);
		receiverAccount.setCredentialExpired(false);
		receiverAccount.setAccountLocked(false);

		User receiverUser = new User();
		receiverUser.setLastName("Doe");
		receiverUser.setId(123L);
		receiverUser.setAccount(receiverAccount);
		receiverUser.setFirstName("Jane");

		receiverWallet = new Wallet();
		receiverWallet.setCurrency(null);
		receiverWallet.setBalance(10.0);
		receiverWallet.setUser(receiverUser);
		receiverWallet.setId(123L);

		receiverBankAccount = new BankAccount();
		receiverBankAccount.setSwiftCode("Swift Code");
		receiverBankAccount.setUser(receiverUser);
		receiverBankAccount.setId(123L);
		receiverBankAccount.setOwnerFullName("Dr Jane Doe");
		receiverBankAccount.setIban("Iban");

		transaction = new Transaction();
		transaction.setDate(LocalDate.ofEpochDay(1L));
		transaction.setReceiverWallet(receiverWallet);
		transaction.setReceiverBankAccount(receiverBankAccount);
		transaction.setId(123L);
		transaction.setCommissionAmount(10.0);
		transaction.setDescription("The characteristics of someone or something");
		transaction.setSenderWallet(senderWallet);

		transactionRequest = new TransactionRequest();
		transactionRequest.setReceiverUserId(123L);
		transactionRequest.setSenderUserId(123L);
		transactionRequest.setAmount(10.0);
		transactionRequest.setDescription("The characteristics of someone or something");
		transactionRequest.setIsWalletRecharge(true);
	}

	@Test
	void testSave_shouldReturnTheCreatedTransaction_forAValidSenderAndReceiver() throws BadArgument,
			EmptySearchResult {
		when(this.iWalletService.getOneByUserId((Long) any())).thenReturn(senderWallet);
		when(this.iWalletDAO.save((Wallet) any())).thenReturn(receiverWallet);
		when(this.iTransactionDAO.save((Transaction) any())).thenReturn(transaction);
		when(this.iBankAccountService.getOneByUserId((Long) any())).thenReturn(receiverBankAccount);
		assertSame(transaction, this.transactionService.save(transactionRequest));
		verify(this.iWalletService).getOneByUserId((Long) any());
		verify(this.iWalletDAO).save((Wallet) any());
		verify(this.iTransactionDAO).save((Transaction) any());
		verify(this.iBankAccountService).getOneByUserId((Long) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldThrowAnEmptySearchResult_forAnEmptyQueryResult() throws
			EmptySearchResult {
		Optional<Wallet> ofResult = Optional.<Wallet>of(senderWallet);
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(ofResult);
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(new HashSet<Transaction>());
		assertThrows(EmptySearchResult.class, () -> this.transactionService.getAllTransactionsForAccount(123L));
		verify(this.iWalletService).findWalletByUserId((Long) any());
		verify(this.iTransactionDAO).findAllBySenderWallet((Wallet) any(),
				(org.springframework.data.domain.Sort) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldReturnASetOfTransactions_forAValidSenderWallet() throws
			EmptySearchResult,
			BadArgument {
		Optional<Wallet> ofResult = Optional.<Wallet>of(senderWallet);
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(ofResult);
		HashSet<Transaction> transactionSet = new HashSet<Transaction>();
		transactionSet.add(transaction);
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(transactionSet);
		Set<Transaction> actualAllTransactionsForAccount = this.transactionService.getAllTransactionsForAccount(123L);
		assertSame(transactionSet, actualAllTransactionsForAccount);
		assertEquals(1, actualAllTransactionsForAccount.size());
		verify(this.iWalletService).findWalletByUserId((Long) any());
		verify(this.iTransactionDAO).findAllBySenderWallet((Wallet) any(),
				(org.springframework.data.domain.Sort) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldThrowABadArgument_forAnNonFoundSenderWallet() throws BadArgument {
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(Optional.<Wallet>empty());
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(new HashSet<Transaction>());
		assertThrows(BadArgument.class, () -> this.transactionService.getAllTransactionsForAccount(123L));
		verify(this.iWalletService).findWalletByUserId((Long) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldThrowAnEmptySearchResultException_forAnEmptyQueryResult() throws
			EmptySearchResult {
		Optional<Wallet> ofResult = Optional.<Wallet>of(senderWallet);
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(ofResult);
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(new HashSet<Transaction>());
		assertThrows(EmptySearchResult.class, () -> this.transactionService.getAllTransactionsForAccount(123L));
		verify(this.iWalletService).findWalletByUserId((Long) any());
		verify(this.iTransactionDAO).findAllBySenderWallet((Wallet) any(),
				(org.springframework.data.domain.Sort) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldThrowAUsernameNotFoundException_forANonExistingAccount() throws
			BadArgument {
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(Optional.<Wallet>empty());
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(new HashSet<Transaction>());
		assertThrows(BadArgument.class, () -> this.transactionService.getAllTransactionsForAccount(123L));
		verify(this.iWalletService).findWalletByUserId((Long) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldReturnASetOfTransactions_forAnExistingAccount() throws
			EmptySearchResult, BadArgument {
		Optional<Wallet> ofResult = Optional.<Wallet>of(senderWallet);
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(ofResult);
		HashSet<Transaction> transactionSet = new HashSet<Transaction>();
		transactionSet.add(transaction);
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(transactionSet);
		Set<Transaction> actualAllTransactionsForAccount = this.transactionService.getAllTransactionsForAccount(123L);
		assertSame(transactionSet, actualAllTransactionsForAccount);
		assertEquals(1, actualAllTransactionsForAccount.size());
		verify(this.iWalletService).findWalletByUserId((Long) any());
		verify(this.iTransactionDAO).findAllBySenderWallet((Wallet) any(),
				(org.springframework.data.domain.Sort) any());
	}

	@Test
	void testGetAllTransactionsForAccount_shouldThrowAnEmptySearchResult_forNoTransactionsFound() throws
			EmptySearchResult {
		Optional<Wallet> ofResult = Optional.<Wallet>of(senderWallet);
		when(this.iWalletService.findWalletByUserId((Long) any())).thenReturn(ofResult);
		when(this.iTransactionDAO.findAllBySenderWallet((Wallet) any(), (org.springframework.data.domain.Sort) any()))
				.thenReturn(new HashSet<Transaction>());
		assertThrows(EmptySearchResult.class, () -> this.transactionService.getAllTransactionsForAccount(123L));
		verify(this.iWalletService).findWalletByUserId((Long) any());
		verify(this.iTransactionDAO).findAllBySenderWallet((Wallet) any(),
				(org.springframework.data.domain.Sort) any());
	}

	@Test
	void testRechargeWallet_shouldReturnTheSavedTransaction_forAValidSenderWalletAndReceiverWalletOrBankAccount() throws
			EmptySearchResult {
		when(this.iWalletService.getOneByUserId((Long) any())).thenReturn(senderWallet);
		when(this.iWalletDAO.save((Wallet) any())).thenReturn(senderWallet);

		when(this.iTransactionDAO.save((Transaction) any())).thenReturn(transaction);
		when(this.iBankAccountService.getOneByUserId((Long) any())).thenReturn(receiverBankAccount);

		TransactionRequest transactionRequest = new TransactionRequest();
		transactionRequest.setReceiverUserId(123L);
		transactionRequest.setSenderUserId(123L);
		transactionRequest.setAmount(10.0);
		transactionRequest.setDescription("The characteristics of someone or something");
		transactionRequest.setIsWalletRecharge(true);
		assertSame(transaction, this.transactionService.rechargeWallet(transactionRequest));
		verify(this.iWalletService).getOneByUserId((Long) any());
		verify(this.iWalletDAO).save((Wallet) any());
		verify(this.iTransactionDAO).save((Transaction) any());
		verify(this.iBankAccountService).getOneByUserId((Long) any());
	}

	@Test
	void testGetLastMonthSentAmountBySenderWalletId_shouldReturnTheSentAmount_forAValidWalletId() {
		when(this.iWalletService.getOneByUserId((Long) any())).thenReturn(senderWallet);
		when(this.iTransactionDAO.getLastMonthSentAmountBySenderWalletId((Long) any()))
				.thenReturn(Optional.<Double>of(10.0));
		assertEquals(10.0, this.transactionService.getLastMonthSentAmountBySenderWalletId(123L).doubleValue());
		verify(this.iWalletService).getOneByUserId((Long) any());
		verify(this.iTransactionDAO).getLastMonthSentAmountBySenderWalletId((Long) any());
	}

	@Test
	void testGetLastMonthReceivedAmountBySenderWalletId_shouldReturnTheReceivedAmount_forAValidWalletId() {
		when(this.iWalletService.getOneByUserId((Long) any())).thenReturn(senderWallet);
		when(this.iTransactionDAO.getLastMonthReceivedAmountBySenderWalletId((Long) any()))
				.thenReturn(Optional.<Double>of(10.0));
		assertEquals(10.0, this.transactionService.getLastMonthReceivedAmountBySenderWalletId(123L).doubleValue());
		verify(this.iWalletService).getOneByUserId((Long) any());
		verify(this.iTransactionDAO).getLastMonthSentAmountBySenderWalletId((Long) any());
	}

	@Test
	void testGetLastMonthBankTransferredAmountBySenderWalletId_shouldReturnTheTransferredAmount_forAValidWalletId() {
		when(this.iWalletService.getOneByUserId((Long) any())).thenReturn(senderWallet);
		when(this.iTransactionDAO.getLastMonthBankTransferredAmountBySenderWalletId((Long) any()))
				.thenReturn(Optional.<Double>of(10.0));
		assertEquals(10.0,
				this.transactionService.getLastMonthBankTransferredAmountBySenderWalletId(123L).doubleValue());
		verify(this.iWalletService).getOneByUserId((Long) any());
		verify(this.iTransactionDAO).getLastMonthSentAmountBySenderWalletId((Long) any());
	}
}
