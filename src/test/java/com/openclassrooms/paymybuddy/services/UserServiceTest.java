package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.*;
import com.openclassrooms.paymybuddy.repository.IUserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UserService.class})
@ExtendWith(SpringExtension.class)
class UserServiceTest {

	@MockBean
	private IUserDAO iUserDAO;

	@Autowired
	private UserService userService;

	private Account account;

	private User user;

	private SignUpRequest signUpRequest;

	@BeforeEach
	void setUp() {
		account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		Roles roles = Roles.USER;
		account.setRoles(roles);
		account.setAccountEnabled(true);
		account.setAccountExpired(false);
		account.setCredentialExpired(false);
		account.setAccountLocked(false);

		user = new User();
		user.setId(123L);
		user.setFirstName("Jane");
		user.setLastName("Doe");
		user.setAccount(account);

		BankAccount bankAccount = new BankAccount();
		bankAccount.setId(123L);
		bankAccount.setIban("FR98 8756 7656 3454 6546");
		bankAccount.setSwiftCode("FERVEERGDAD");
		bankAccount.setOwnerFullName("Dr Jane Doe");
		bankAccount.setUser(user);

		signUpRequest = new SignUpRequest();
		signUpRequest.setFirstName("Jane");
		signUpRequest.setLastName("Doe");
		signUpRequest.setPassword("iloveyou");
		signUpRequest.setPasswordConfirmation("iloveyou");
		signUpRequest.setUsername("jane.doe@example.org");
		signUpRequest.setBankAccount(bankAccount);
	}

	@Test
	void testSave_shouldReturnTheSavedUser_forAValidSignUpRequestAndAccount() {
		when(this.iUserDAO.save((User) any())).thenReturn(user);
		assertSame(user, this.userService.save(signUpRequest, account));
		verify(this.iUserDAO).save((User) any());
	}

	@Test
	void testFindOneById_shouldReturnTheFoundUser_forAValidUserId() {
		Optional<User> optionalUser = Optional.<User>of(user);
		when(this.iUserDAO.findById((Long) any())).thenReturn(optionalUser);
		Optional<User> actualFindOneByIdResult = this.userService.findOneById(123L);
		assertSame(optionalUser, actualFindOneByIdResult);
		assertTrue(actualFindOneByIdResult.isPresent());
		verify(this.iUserDAO).findById((Long) any());
	}

	@Test
	void testFindOneByAccount_shouldReturnTheFoundUser_forAValidAccount() {
		Optional<User> expectedUser = Optional.<User>of(user);
		when(this.iUserDAO.findOneByAccount((Account) any())).thenReturn(expectedUser);
		Optional<User> actualFindOneByAccountResult = this.userService.findOneByAccount(account);
		assertSame(expectedUser, actualFindOneByAccountResult);
		assertTrue(actualFindOneByAccountResult.isPresent());
		verify(this.iUserDAO).findOneByAccount((Account) any());
	}
}
