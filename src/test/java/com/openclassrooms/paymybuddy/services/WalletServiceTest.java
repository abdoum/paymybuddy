package com.openclassrooms.paymybuddy.services;

import com.openclassrooms.paymybuddy.model.Account;
import com.openclassrooms.paymybuddy.model.Roles;
import com.openclassrooms.paymybuddy.model.User;
import com.openclassrooms.paymybuddy.model.Wallet;
import com.openclassrooms.paymybuddy.repository.IWalletDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {WalletService.class})
@ExtendWith(SpringExtension.class)
class WalletServiceTest {

	@MockBean
	IWalletDAO iWalletDAO;

	@Autowired
	WalletService walletService;

	private Wallet wallet;

	private User user;

	@BeforeEach
	void setUp() {
		Account account = new Account();
		account.setEmail("jane.doe@example.org");
		account.setPassword("iloveyou");
		account.setId(123L);
		account.setRoles(Roles.USER);
		account.setAccountEnabled(true);
		account.setAccountExpired(true);
		account.setCredentialExpired(true);
		account.setAccountLocked(true);

		user = new User();
		user.setLastName("Doe");
		user.setId(123L);
		user.setAccount(account);
		user.setFirstName("Jane");

		wallet = new Wallet();
		wallet.setCurrency(null);
		wallet.setBalance(10.0);
		wallet.setUser(user);
		wallet.setId(123L);
	}

	@Test
	void testCreate_shouldReturnTheSavedWalled_forAValidWallet() {
		when(this.iWalletDAO.save((Wallet) any())).thenReturn(wallet);
		assertSame(wallet, this.walletService.save(user));
		verify(this.iWalletDAO).save((Wallet) any());
	}

	@Test
	void testFindWalletByUserId_shouldReturnTheFoundWalletOrNull_forAValidUserId() {
		Optional<Wallet> ofResult = Optional.<Wallet>of(wallet);
		when(this.iWalletDAO.findByUserId((Long) any())).thenReturn(ofResult);
		Optional<Wallet> actualFindWalletByUserIdResult = this.walletService.findWalletByUserId(123L);
		assertSame(ofResult, actualFindWalletByUserIdResult);
		assertTrue(actualFindWalletByUserIdResult.isPresent());
		verify(this.iWalletDAO).findByUserId((Long) any());
	}

	@Test
	void testGetOneByUserId_shouldReturnTheWallet_forAValidUserId() {
		when(this.iWalletDAO.getOneByUserId((Long) any())).thenReturn(wallet);
		assertSame(wallet, this.walletService.getOneByUserId(123L));
		verify(this.iWalletDAO).getOneByUserId((Long) any());
	}
}
