START TRANSACTION;
CREATE SCHEMA IF NOT EXISTS `PayMyBuddy` DEFAULT CHARACTER SET utf8;

use `PayMyBuddy`;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `first_name` VARCHAR(50) NULL,
  `last_name` VARCHAR(50) NULL,
  `account` int NOT NULL UNIQUE,
  PRIMARY KEY (`user_id`)
)
engine = innoDB;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`account` (
  `account_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `email` VARCHAR(254) NULL UNIQUE,
  `password` TINYTEXT NULL,
  `account_expired` tinyint DEFAULT 0,
  `account_locked` tinyint NULL DEFAULT 0,
  `credential_expired` tinyint NULL DEFAULT 0,
  `account_enabled` tinyint NULL DEFAULT 1,
  `roles` VARCHAR(250) NULL DEFAULT 'USER',
  PRIMARY KEY (`account_id`),
  UNIQUE INDEX `email_unique` (`email` ASC) visible
)
engine = innoDB;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`wallet` (
  `wallet_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `balance` DECIMAL(9,2) default 0,
  `currency` CHAR(3) default 'EUR',
  `user_id` INT NOT NULL,
  PRIMARY KEY (`wallet_id`)
)
engine = innoDB;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`bank_account` (
  `bank_account_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  `iban` VARCHAR(37) NOT NULL,
  `swift_code` VARCHAR(11) NOT NULL,
  `owner_full_name` VARCHAR(100) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`bank_account_id`)
)
engine = innoDB;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`connection` (
  `source_user_id` INT NOT NULL,
  `target_user_id` INT NOT NULL,
  PRIMARY KEY (`source_user_id`, `target_user_id`)
)
engine = innoDB;

CREATE TABLE IF NOT EXISTS `PayMyBuddy`.`transaction` (
`transaction_id` INT NOT NULL AUTO_INCREMENT UNIQUE,
`date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
`amount` DECIMAL(9,2) NOT NULL,
`description` VARCHAR(250) NULL,
`commission_amount` DECIMAL(9,2) NOT NULL,
`sender_wallet_id` INT NULL DEFAULT NULL,
`receiver_wallet_id` INT NULL DEFAULT NULL,
`receiver_bank_account_id` INT NULL DEFAULT NULL,
PRIMARY KEY (`transaction_id`),
constraint sender_wallet_or_receiver_bank_account check
    (
     receiver_bank_account_id IS NOT NULL
     OR receiver_wallet_id IS NOT NULL
    )
)
engine = innoDB;

ALTER TABLE `PayMyBuddy`.`user`
ADD CONSTRAINT `fk_user_account_id`
  FOREIGN KEY (`account_id`)
  REFERENCES `PayMyBuddy`.`account` (`account_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `PayMyBuddy`.`wallet`
ADD CONSTRAINT `fk_wallet_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `PayMyBuddy`.`user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `PayMyBuddy`.`bank_account`
ADD CONSTRAINT `fk_bank_account_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `PayMyBuddy`.`user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `PayMyBuddy`.`connection`
ADD CONSTRAINT `fk_connection_source_user_id`
  FOREIGN KEY (`source_user_id`)
  REFERENCES `PayMyBuddy`.`user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_connection_target_user_id`
  FOREIGN KEY (`target_user_id`)
  REFERENCES `PayMyBuddy`.`user` (`user_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `PayMyBuddy`.`transaction`
ADD CONSTRAINT `fk_transaction_sender_wallet_id`
	FOREIGN KEY (`sender_wallet_id`)
	REFERENCES `PayMyBuddy`.`wallet` (`wallet_id`)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
ADD	CONSTRAINT `fk_transaction_receiver_wallet_id`
	FOREIGN KEY (`receiver_wallet_id`)
	REFERENCES `PayMyBuddy`.`wallet` (`wallet_id`)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
COMMIT;
